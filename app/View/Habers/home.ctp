<?php
foreach($habers as $row){
?>
<blockquote class="bg-warning">
<div class="row" style="margin-bottom: 1%">
    <?php
    $link = $this->Html->url('/').'habers/haber/ilanNo:'.$row['Haber']['id'];
     ?>
    <a href="<?php echo $link;?>">
    <div class="col-xs-6 col-sm-5 text-center">
         <?php 
         if($haberRes[$row['Haber']['id']]){
             echo '<img class="imgHome" src="'.$this->Html->url('/').$haberRes[$row['Haber']['id']][0]['HaberResim']['path'].'"  alt="'.$row['Haber']['baslik'].' '.$haberRes[$row['Haber']['id']][0]['HaberResim']['id'].'"/>';
        }else{
            echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png" alt="'.$row['Haber']['baslik'].'"/>';
        }
        ?>
    </div>
    </a>
    <div class="col-xs-6 col-sm-7">
        <!--<div class="row">-->
            <div class="fontBold" style="color:#000000"><?php echo CakeText::truncate($row['Haber']['baslik'],50);?></div>
            <div class=""><?php echo CakeText::truncate(strip_tags($row['Haber']['aciklama']),150);?></div>
            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="fRight" style="text-align: right">
                <a href="<?php echo $link;?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>admins/haberedit?id=<?php echo $row['Haber']['id'];?>" class="btn  btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <!--<button type="button" class="btn  btn-danger btn-sm" onclick="HaberSil(<?php echo $row['Haber']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>-->
            </div>
            <?php
            }else{
            ?>
            <div class="fRight" style="text-align: right">
                <a href="<?php echo $link;?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        <!--</div>-->
    </div>
</div>
</blockquote>
<?php
}
// pagination section
    echo '<ul class="pagination" style="display:table; margin:0 auto;">';
 
        echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
        echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        
    echo '</ul>'; 
?>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "HABER - Maden Gayrimenkul";
});
</script>