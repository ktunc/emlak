<?php
//echo $this->Html->script('locationpicker.jquery');
//echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&libraries=places');
?>
<div class="container">
<h3>Yeni Haber</h3>
<form method="POST" role="form" class="form-signin" action="<?php echo $this->Html->url('/');?>habers/addHaber" id="HaberForm" enctype="multipart/form-data">
    <label for="baslik">Başlık</label>
    <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control">
    <label for="aciklama">Açıklama</label>
    <textarea name="aciklama" id="aciklama" placeholder="Açıklama" class="form-control" rows="10"></textarea>
    <br>
    <!-- Video -->
    <label for="video">Video</label>
    <table id="videolar"></table>
    <button class="btn btn3d btn-warning" type="button" id="addVideo">Video Ekle</button>
    <br>
    <!-- Resim -->
    <label for="resim">Resim</label>
    <table id="resler">
        
    </table>
    <button class="btn btn3d btn-warning" type="button" id="addResim">Resim Ekle</button>
    <br>
    
<!--    <label for="us2-address">Location</label>
    <input type="text" id="us2-address" name="location"  class="form-control"/>
    <div id="us2" style="min-height: 300px;margin-top: 2%;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
    <input type="hidden" id="us2-lat" name="latitude"/>
    <input type="hidden" id="us2-lon" name="longitude"/>-->
    <br>
    
    <div class="btn btn3d btn-group">
        <button type="button" class="btn btn3d btn-primary" id="HaberKaydet">Kaydet</button>
        <button type="reset" class="btn btn3d btn-danger">Sıfırla</button>
    </div>
</form>
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
var resimTR = '<tr><td><input type="file" name="haberRes[]" class="form-control"/></td>\n\
<td><button type="button" class="btn btn3d btn-danger" id="resIptal">İptal</button></td></tr>';
$(document).ready(function(){
    tinymce.init({
        selector: '#aciklama',
        language: 'tr_TR',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });

    $('#addResim').on('click',function(){
        if($('input[name="haberRes[]"]').length >= 20){
            alert('20 taneden fazla resim eklenemez.');
        }else{
            $('#resler').append(resimTR);
        }
    });
    
    $('#addVideo').on('click',function(){
        $('#videolar').append('<tr><td><input type="file" name="haberVideo" class="form-control"/></td>\n\
<td><button type="button" class="btn btn3d btn-danger btn-small" id="videoIptal">İptal</button></td></tr>');
        $('#addVideo').hide();
    });
    
    $('#videolar').on('click','#videoIptal',function(){
        $(this).closest('tr').remove();
        $('#addVideo').show();
    });
    
    $('#resler').on('click','#resIptal',function(){
       $(this).closest('tr').remove();
    });
    
    $('#HaberKaydet').on('click',function(){
        if($('#baslik').val() == ''){
            alert('Lütfen başlık giriniz.');
        }
        else{
            $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
            $('#HaberForm').submit();
        }
    });
    
//    $('#us2').locationpicker({
//	location: {latitude: 39.918012967883385, longitude: 32.85808648203124},	
//	radius: 10,
//	inputBinding: {
//        latitudeInput: $('#us2-lat'),
//        longitudeInput: $('#us2-lon'),
//        radiusInput: $('#us2-radius'),
//        locationNameInput: $('#us2-address')
//        },
//        enableAutocomplete: true
//    });
});   
</script>