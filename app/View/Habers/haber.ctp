<?php 
echo $this->Html->css('photoswipe');
echo $this->Html->script('simple-inheritance.min');
echo $this->Html->script('code-photoswipe-1.0.11');
echo $this->Html->css('../js/Flickerplate-master/css/flickerplate');
echo $this->Html->script('Flickerplate-master/js/min/modernizr-custom-v2.7.1.min');
echo $this->Html->script('Flickerplate-master/js/min/jquery-finger-v0.1.0.min');
echo $this->Html->script('Flickerplate-master/js/min/flickerplate.min');
?>

<div class="bg-white printwidth">
<div class="ilanUst row">
    <div class="col-xs-12 text-danger h3 fontBold"><?php echo $habers['Haber']['baslik'];?></div>
    <div class="h5 col-xs-12"><?php echo generateTarih($habers['Haber']['tarih']);?></div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="flicker-example flickCSS" data-block-text="false">
            <ul id="Gallery">
                <?php
                if(count($habers['HaberResim'])>0){
                    foreach($habers['HaberResim'] as $row){
                         echo '<li style="background-size:100% 100%">'
                                 .'<div class="flick-title">'
                                 . '<a href="'.$this->Html->url('/').$row['path'].'">'
                                 . '<img class="imgIcerik" src="'.$this->Html->url('/').$row['path'].'" alt="'.$habers['Haber']['baslik'].' '.$row['id'].'"/>'
                                 . '</a>'
                                 . '</div>'
                                 . '</li>';
                    }
                }else{
                     echo '<li style="background-size:100% 100%">'
                        .'<div class="flick-title">'
                        . '<a href="'.$this->Html->url('/').'img/logo.png">'
                        . '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png" alt="'.$habers['Haber']['baslik'].'"/>'
                        . '</a>'
                        . '</div>'
                        . '</li>';
                } ?>
            </ul>
            <div class="IlanShareIcon">
                <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="haberId" value="<?php echo $habers['Haber']['id']; ?>" />

    <div class="row">
        <div class="ilanIcerik col-xs-12" style="text-align: left;">
            <?php echo nl2br($habers['Haber']['aciklama']);?>
        </div>
    </div>

</div>
<!--<div class="col-xs-12" id="us2" style="min-width: 300px; min-height: 300px;margin-top: 2%;" ></div>-->
<?php 
function generateTarih($tarih){
   return date('d.m.Y H:i:s',strtotime($tarih));
}
?>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "<?php echo $habers['Haber']['baslik'];?>";

    
Code.photoSwipe('a', '#Gallery');

$('.flicker-example').flicker({
    auto_flick: true,
    auto_flick_delay: 5,
    flick_animation: "transform-slide"
});

    $('#ShareBut').on('click',function(){
        $('#ShareIlanModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
});
</script>