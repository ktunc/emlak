<?php
echo $this->Html->css('jquery.filer');
echo $this->Html->css('themes/jquery.filer-dragdropbox-theme');
echo $this->Html->script('locationpicker.jquery');
echo $this->Html->script('http://maps.google.com/maps/api/js?key=AIzaSyBb6wy1FSr2ms69Cy7BSuZQLOB9-EPIkIA&sensor=false&libraries=places');
echo $this->Html->script('jquery.filer.min');
echo $this->Html->script('jquery.filer.custom');
$ilanOzellik = array();
foreach ($ilan['IlanOzellik'] as $ozl){
    $ilanOzellik[] = $ozl['ozellik_id'];
}

$sehir = '<option value="0">Seçiniz</option>';
foreach ($il as $row) {
    if($row['Sehir']['id'] == $ilan['Ilan']['sehir_id']){
        $sehir .= '<option selected="selected" value="'.$row['Sehir']['id'].'">'.$row['Sehir']['sehir_adi'].'</option>';
    }else{
        $sehir .= '<option value="'.$row['Sehir']['id'].'">'.$row['Sehir']['sehir_adi'].'</option>';
    }
}
$ilceler = '';

$ilceler = '<option value="0">Seçiniz</option>';
foreach ($ilce as $row) {
    if($row['Ilce']['id'] == $ilan['Ilan']['ilce_id']){
        $ilceler .= '<option selected="selected" value="'.$row['Ilce']['id'].'">'.$row['Ilce']['ilce_adi'].'</option>';
    }else{
        $ilceler .= '<option value="'.$row['Ilce']['id'].'">'.$row['Ilce']['ilce_adi'].'</option>';
    }
}

$semtler='';
if($ilan['Ilan']['ilce_id']){
    $semtler = '<option value="0">Seçiniz</option>';
    foreach ($semt as $row) {
        if($row['Semt']['id'] == $ilan['Ilan']['semt_id']){
            $semtler .= '<option selected="selected" value="'.$row['Semt']['id'].'">'.$row['Semt']['semt_adi'].'</option>';
        }else{
            $semtler .= '<option value="'.$row['Semt']['id'].'">'.$row['Semt']['semt_adi'].'</option>';
        }
    }
}
$mahalleler='';
if($ilan['Ilan']['semt_id']){
    $mahalleler = '<option value="0">Seçiniz</option>';
    foreach ($mahalle as $row) {
        if($row['Mahalle']['id'] == $ilan['Ilan']['mahalle_id']){
            $mahalleler .= '<option selected="selected" value="'.$row['Mahalle']['id'].'">'.$row['Mahalle']['mahalle_adi'].'</option>';
        }else{
            $mahalleler .= '<option value="'.$row['Mahalle']['id'].'">'.$row['Mahalle']['mahalle_adi'].'</option>';
        }
    }
}

$satkir = '';
if(1 == $ilan['Ilan']['sat_kir']){
    $satkir .= '<option value="1" selected="selected">Satılık</option>';
    $satkir .= '<option value="2">Kiralık</option>';
}else if(2 == $ilan['Ilan']['sat_kir']){
    $satkir .= '<option value="1">Satılık</option>';
    $satkir .= '<option value="2" selected="selected">Kiralık</option>';
}

$danismanSelect = '<option value="0">Seçiniz</option>';
foreach ($danismanlar as $row){
    $danismanSelect .= '<option value="'.$row['Danisman']['id'].'">'.$row['Danisman']['adi'].' '.$row['Danisman']['soyadi'].'</option>';
}
$ilantiparr = array(1=>'Konut', 2=>'İşyeri', 3=>'Arsa');
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?=$ilan['Ilan']['ilan_no'].' İlan Numaralı '.$ilantiparr[$ilan['Ilan']['ilan_tipi']]?> Düzenle</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" role="form" class="form-horizontal" action="<?php echo $this->Html->url('/');?>admins/ilankaydet" id="IlanForm" enctype="multipart/form-data">
                    <input type="hidden" name="ilan_tipi" value="<?=$ilan['Ilan']['ilan_tipi']?>"/>
                    <input type="hidden" name="ilan_id" value="<?=$ilan['Ilan']['id']?>"/>
                    <div class="form-group">
                        <label for="baslik" class="col-xs-3 col-md-2 control-label">Başlık</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control" value="<?=$ilan['Ilan']['baslik']?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aciklama" class="col-xs-3 col-md-2 control-label">Açıklama</label>
                        <div class="col-xs-9 col-md-10">
                            <textarea name="aciklama" rows="5" id="aciklama" placeholder="Açıklama" class="form-control"><?=$ilan['Ilan']['aciklama']?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="il" class="col-xs-3 col-md-2 control-label">Şehir</label>
                        <div class="col-xs-9 col-md-10">
                            <select id="il" name="il" class="form-control"><?php echo $sehir; ?></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ilce" class="col-xs-3 col-md-2 control-label">İlçe</label>
                        <div class="col-xs-9 col-md-10">
                            <select id="ilce" name="ilce" class="form-control"><?php echo $ilceler; ?></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="semt" class="col-xs-3 col-md-2 control-label">Semt</label>
                        <div class="col-xs-9 col-md-10">
                            <select id="semt" name="semt" class="form-control"><?php echo $semtler; ?></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mahalle" class="col-xs-3 col-md-2 control-label">Mahalle</label>
                        <div class="col-xs-9 col-md-10">
                            <select id="mahalle" name="mahalle" class="form-control"><?php echo $mahalleler; ?></select>
                        </div>
                    </div>
                    <?php
                    if($ilan['Ilan']['ilan_tipi'] == 1){
                        echo $this->element('IlanTip/konut', array('data' => $ilan['KonutBilgi']));
                    }else if($ilan['Ilan']['ilan_tipi'] == 2){
                        echo $this->element('IlanTip/isyeri', array('data' => $ilan['IsyeriBilgi']));
                    }else if($ilan['Ilan']['ilan_tipi'] == 3){
                        echo $this->element('IlanTip/arsa', array('data' => $ilan['ArsaBilgi']));
                    }
                    ?>
                    <div class="form-group">
                        <label for="kredi" class="col-xs-3 col-md-2 control-label">Kredi Uygunluğu</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="kredi" id="kredi" placeholder="Kredi Uygunluğu" class="form-control" value="<?=$ilan['Ilan']['kredi']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fiyat" class="col-xs-3 col-md-2 control-label">Fiyat</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="number" name="fiyat" id="fiyat" placeholder="Fiyat" class="form-control" value="<?=$ilan['Ilan']['fiyat']?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sat_kir" class="col-xs-3 col-md-2 control-label">Satılık - Kiralık</label>
                        <div class="col-xs-9 col-md-10">
                            <select id="sat_kir" name="sat_kir" class="form-control"><?php echo $satkir;?></select>
                        </div>
                    </div>
                    <!-- Mal Sahibi Bilgileri -->
                    <div class="form-group">
                        <label for="malName" class="col-xs-3 col-md-2 control-label">Mal Sahibi Adı Soyadı</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="malName" id="malName" placeholder="Mal Sahibi Adı Soyadı" class="form-control" value="<?php echo $ilan['Ilan']['mal_name'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="malTelNo" class="col-xs-3 col-md-2 control-label">Mal Sahibi Telefon No:</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="malTelNo" id="malTelNo" placeholder="Mal Sahibi Telefon No" class="form-control" value="<?php echo $ilan['Ilan']['mal_tel'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="malFiyat" class="col-xs-3 col-md-2 control-label">Mal Sahibi Fiyat</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="number" name="malFiyat" id="malFiyat" placeholder="Mal Sahibi Fiyat" class="form-control" value="<?php echo $ilan['Ilan']['mal_fiyat'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">İlan Özellikleri</a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <?php
                                            foreach ($ozellikler as $row) {
                                                $checked = '';
                                                if(in_array($row['Ozellik']['id'], $ilanOzellik)){
                                                    $checked = ' checked="checked"';
                                                }
                                                echo '<div class="col-xs-12 col-sm-3" style="margin-bottom: 10px;"><input class="icheksOzellik" '.$checked.' type="checkbox" name="ozellik['.$row['Ozellik']['id'].']" id="ozellik_'.$row['Ozellik']['id'].'"/> <label for="ozellik_'.$row['Ozellik']['id'].'">'.$row['Ozellik']['ozellik'].'</label></div>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="danisman" class="control-label col-xs-3 col-md-2 ">Danışmanlar</label>
                        <div class="col-xs-9 col-md-10">
                            <table id="danismanTable" class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Danışman</th>
                                    <th>Sil</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(!empty($ilan['IlanDanisman'])){
                                    foreach($ilan['IlanDanisman'] as $row){
                                        ?>
                                        <tr id="danisman_<?=$row['id']?>">
                                            <td><?=$row['Danisman']['adi'].' '.$row['Danisman']['soyadi']?></td>
                                            <td><button type="button" class="btn btn-xs btn-danger" onclick="FuncDanismanSil(<?=$row['id']?>,'IlanDanisman')">Sil</button></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-md-offset-2"><button type="button" class="btn btn-xs btn-success" id="DanismanEkle"><i class="fa fa-plus"></i> Danışman Ekle</button></div>
                    </div>
                    <div class="form-group"><hr></div>

                    <div class="form-group">
                        <label for="resim" class="control-label col-xs-3 col-md-2 ">Resimler</label>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-md-offset-2 sortable">
                            <?php
                            foreach($ilan['IlanResim'] as $cow){
                                ?>
                                <div class="col-sm-6 col-md-3 resims sort-item" id="Res_<?php echo $cow['id'];?>">
                                    <div class="thumbnail" style="background-color: #FFFFFF" >
                                        <img src="<?php echo $this->Html->url('/').$cow['path'];?>" alt="" style="width:300px;height:200px;" alt="<?php echo $ilan['Ilan']['baslik'].' '.$cow['id']; ?>"/>
                                        <div class="text-right">
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Resmi Sil" onclick="DeleteResim('<?php echo $cow['id'];?>')"><i class="fa fa-trash fa-2x"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <hr>
                    </div>
                    <div class="form-group">
                        <div id="content">
                            <!-- Example 2 -->
                            <input type="file" name="files[]" id="filer_input2" multiple="multiple" accept="image/*">
                            <!-- end of Example 2 -->
                        </div>
                    </div>

                    <div class="form-group">
                        <hr>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 col-md-2 control-label">Harita:</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="checkbox" name="harita" id="harita" class="i-checks" <?=$ilan['Ilan']['harita']==1?'checked="checked"':''?>/> <label for="harita">İlanda haritayı göster.</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="us2-address" class="col-xs-3 col-md-2 control-label">Location</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" id="us2-address" name="location"  class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12" id="us2" style="min-height: 400px;margin-top: 2%;"></div>
                        <input type="hidden" id="us2-lat" name="latitude" />
                        <input type="hidden" id="us2-lon" name="longitude" />
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12" style="margin-top:5%">
                            <button type="button" class="btn btn-sm btn-primary" id="IlanKaydet">Kaydet</button>
                            <button type="button" class="btn btn-sm btn-default" id="IlanIptal">İptal</button>
                            <button type="button" class="btn btn-sm btn-danger" style="float:right" onclick="IlanSil(<?php echo $ilan['Ilan']['id'];?>)">Sil</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script('tinymce/tinymce.min');
echo $this->Html->script('jquery.sortable.min');
?>

<script type="text/javascript">
    var danisman = '<tr><td><select name="danisman[]"><?=$danismanSelect?></select></td><td><button type="button" class="btn btn-xs btn-danger danismanSil">Sil</button></td></tr>';
    $(document).ready(function(){
        tinymce.init({
            selector: '#aciklama',
            language: 'tr_TR',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        });

        $('input.i-checks').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

        $('input.icheksOzellik').each(function () {
            var self = $(this),
                label = self.next(),
                label_text = label.text();

            label.remove();
            self.iCheck({
                checkboxClass:'icheckbox_line-red',
                checkedCheckboxClass:'icheckbox_line-green checked',

                insert:'<div class="icheck_line-icon"></div>'+label_text
            });
        });

        $('#il,#ilce,#semt,#mahalle,.ilanilet').select2({
            width:'100%'
        });
        $('[data-toggle="tooltip"]').tooltip();

        $('#DanismanEkle').on('click',function () {
            $('table#danismanTable tbody').append(danisman);
        });

        $('table#danismanTable tbody').on('click','.danismanSil',function () {
            $(this).closest('tr').remove();
        });

        $('.sortable').sortable({
            items: '.sort-item',
            opacity: 0.7,
            // axis: 'x',
            // handle: 'i.icon-sort',
            update: function() {
                var data = $(this).sortable('toArray');
                $.ajax({
                    type:'POST',
                    url:'<?=$this->Html->url('/')?>admins/resimSirala',
                    data:{'resimler':data, 'tablo':'IlanResim'},
                    beforeSend:function () {
                        $.blockUI();
                    }
                }).done(function (data) {
                    var dat = $.parseJSON(data);
                    if(dat['hata']){
                        $.alert({
                            theme:'modern',
                            type:'red',
                            icon:'fa fa-times',
                            title:'Hata',
                            content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                            onClose:function () {
                                $.unblockUI();
                            }
                        });
                    }else{
                        $.unblockUI();
                    }
                }).fail(function (data) {
                    $.alert({
                        theme:'modern',
                        type:'red',
                        icon:'fa fa-times',
                        title:'Hata',
                        content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                        onClose:function () {
                            $.unblockUI();
                        }
                    });
                });
            }
        });

        $('#il').on('change',function (){
            $.blockUI();
            $('#ilce').html('');
            $('#semt').html('');
            var il = $(this).val();
            if(il != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getIlce",
                    data: 'il='+il,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Ilce']['id']+'">'+vall['Ilce']['ilce_adi']+'</option>';
                            });
                            $('#ilce').html(ekle);
                            $.unblockUI();
                        }
                    }
                });
            }else{
                $.unblockUI();
            }
        });

        $('#ilce').on('change',function (){
            $.blockUI();
            $('#semt').html('');
            $('#mahalle').html('');
            var ilce = $(this).val();
            if(ilce != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getSemt",
                    data: 'ilce='+ilce,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Semt']['id']+'">'+vall['Semt']['semt_adi']+'</option>';
                            });
                            $('#semt').html(ekle);
                            $.unblockUI();
                        }
                    }
                });
            }else{
                $.unblockUI();
            }
        });

        $('#semt').on('change',function (){
            $.blockUI();
            $('#mahalle').html('');
            var semt = $(this).val();
            if(semt != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getMahalle",
                    data: 'semt='+semt,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Mahalle']['id']+'">'+vall['Mahalle']['mahalle_adi']+'</option>';
                            });
                            $('#mahalle').html(ekle);
                            $.unblockUI();
                        }
                    }
                });
            }else{
                $.unblockUI();
            }
        });

        $('#IlanKaydet').on('click',function(){
            if($('#baslik').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    columnClass:'l',
                    icon:'fa fa-exclamation',
                    title:"Lütfen 'Başlık' alanını boş bırakmayınız.",
                    content:''
                });
            }else if($('#il').val() == 0){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    columnClass:'l',
                    icon:'fa fa-exclamation',
                    title:"Lütfen bir şehir seçiniz.",
                    content:''
                });
            } else{
                var formdata = new FormData($('form#IlanForm').get(0));
                $.ajax({
                    type:'POST',
                    url:'<?=$this->Html->url('/')?>admins/ilankaydet',
                    data:formdata,
                    beforeSend:function () {
                        $.blockUI();
                    },
                    processData: false,
                    contentType: false
                }).done(function (data) {
                    var dat = $.parseJSON(data);
                    if(dat['hata']){
                        $.confirm({
                            theme:'modern',
                            type:'danger',
                            icon:'fa fa-close',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        if(dat['link']){
                                            window.location.href = dat['link'];
                                        }
                                    }
                                }
                            }
                        });
                    }else{
                        $.confirm({
                            theme:'modern',
                            type:'green',
                            icon:'fa fa-check',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        window.location.href = dat['link'];
                                    }
                                }
                            }
                        });
                    }
                }).fail(function () {
                    $.alert({
                        theme:'modern',
                        type:'danger',
                        icon:'fa fa-close',
                        title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                        content:'',
                        onContentReady:function () {
                            $.unblockUI();
                        }
                    });
                });
            }
        });

        $('#us2').locationpicker({
            location: {latitude: <?php echo empty($ilan['Ilan']['latitude'])?0:$ilan['Ilan']['latitude'];?>, longitude: <?php echo empty($ilan['Ilan']['longitude'])?0:$ilan['Ilan']['longitude'];?>},
            radius: 10,
            inputBinding: {
                latitudeInput: $('#us2-lat'),
                longitudeInput: $('#us2-lon'),
                radiusInput: $('#us2-radius'),
                locationNameInput: $('#us2-address')
            },
            enableAutocomplete: true
        });

        $('#IlanIptal').on('click',function(){
            window.location.href = '<?php echo $this->Html->url('/');?>admins/ilanlar';
        });
    });

    function DeleteResim(resId){
        $.confirm({
            type:'orange',
            theme:'modern',
            icon:'fa fa-question',
            title:'Resmi silmek istediğinizden emin misiniz?',
            content:'',
            buttons:{
                iptal:{
                    text:'İptal'
                },
                sil:{
                    text:'<i class="fa fa-trash"></i> Sil',
                    btnClass:'btn-danger',
                    action:function(){
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo $this->Html->url('/');?>admins/ilandeleteresim",
                            data: 'resId='+resId,
                            beforeSend:function () {
                                $.blockUI();
                            }
                        }).done(function (data) {
                            var dat = $.parseJSON(data);
                            if(dat['hata']){
                                $.alert({
                                    theme:'modern',
                                    type:'red',
                                    icon:'fa fa-close',
                                    title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    }
                                });
                            }else{
                                $.alert({
                                    theme:'modern',
                                    type:'green',
                                    icon:'fa fa-check',
                                    title:'Resim başarıyla silindi.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    }
                                });
                                $('#Res_'+resId).remove();
                            }
                        }).fail(function () {
                            $.alert({
                                theme:'modern',
                                type:'red',
                                icon:'fa fa-close',
                                title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                content:'',
                                onContentReady:function(){
                                    $.unblockUI();
                                }
                            });
                        });
                    }
                }
            }
        });
    }

    function FuncDanismanSil(dId, tablo) {
        $.ajax({
            type:'POST',
            url:'<?=$this->Html->url('/')?>admins/ilandandanismansil',
            data:{'dId':dId, 'tablo':tablo},
            beforeSend:function () {
                $.blockUI();
            }
        }).done(function (data) {
            var dat = $.parseJSON(data);
            if(dat['hata']){

            }else{
                $('table#danismanTable tr#danisman_'+dId).remove();
                $.unblockUI();
            }
        }).fail(function () {

        });
    }

    function IlanSil(ilanId){
        $.confirm({
            type:'orange',
            theme:'modern',
            icon:'fa fa-question',
            title:'İlanı silmek istediğinizden emin misiniz?',
            content:'',
            buttons:{
                iptal:{
                    text:'İptal'
                },
                sil:{
                    text:'<i class="fa fa-trash"></i> Sil',
                    btnClass:'btn-danger',
                    action:function(){
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo $this->Html->url('/');?>admins/ilansil",
                            data:"ilanId="+ilanId,
                            beforeSend:function () {
                                $.blockUI();
                            }
                        }).done(function (data) {
                            var dat = $.parseJSON(data);
                            if(dat['hata']){
                                $.alert({
                                    theme:'modern',
                                    type:'red',
                                    icon:'fa fa-close',
                                    title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    }
                                });
                            }else{
                                $.alert({
                                    theme:'modern',
                                    type:'green',
                                    icon:'fa fa-check',
                                    title:'İlan başarıyla silindi.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    },
                                    onClose:function(){
                                        window.location.href = "<?=$this->Html->url('/')?>admins/ilanlar";
                                    }
                                });
                            }
                        }).fail(function () {
                            $.alert({
                                theme:'modern',
                                type:'red',
                                icon:'fa fa-close',
                                title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                content:'',
                                onContentReady:function(){
                                    $.unblockUI();
                                }
                            });
                        });
                    }
                }
            }
        });
    }
</script>