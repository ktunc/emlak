<?php
echo $this->Html->css('jquery.filer');
echo $this->Html->css('themes/jquery.filer-dragdropbox-theme');
echo $this->Html->script('jquery.filer.min');
echo $this->Html->script('jquery.filer.danisman');

$iletisimselect = '<option value="0">Seçiniz</option>';
foreach ($IlanIletisimType as $row){
    $iletisimselect .= '<option value="'.$row['IlanIletisimType']['id'].'">'.$row['IlanIletisimType']['name'].'</option>';
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Yeni Danışman</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" role="form" class="form-horizontal" action="<?php echo $this->Html->url('/');?>admins/danismanadd" id="DanismanForm" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="adi" class="col-xs-3 col-md-2 control-label">Adı</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="adi" id="adi" placeholder="Adı" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="soyadi" class="col-xs-3 col-md-2 control-label">Soyadı</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="soyadi" id="soyadi" placeholder="Soyadı" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hakkinda" class="col-xs-3 col-md-2 control-label">Hakkında</label>
                        <div class="col-xs-9 col-md-10">
                            <textarea name="hakkinda" id="hakkinda" placeholder="Hakkında" class="form-control" rows="10"></textarea>
                        </div>
                    </div>

                    <!-- Resim -->
                    <div class="form-group">
                        <label for="resim" class="control-label col-xs-3 col-md-2 ">Resim</label>
                        <div class="col-xs-9 col-md-10">
                            <div id="content">
                                <!-- Example 2 -->
                                <input type="file" name="files[]" id="filer_input2" multiple="multiple" accept="image/*">
                                <!-- end of Example 2 -->
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <hr>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 col-md-2 control-label">İletişim Bilgisi</label>
                    </div>
                    <div id="iletisimDiv">

                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-md-offset-2"><button type="button" class="btn btn-success" id="IletisimEkle"><i class="fa fa-plus"></i> İletişim Bilgisi Ekle</button></div>
                    </div>
                    <div class="form-group">
                        <hr>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-sm-offset-2">
                            <div class="col-xs-6"><button type="reset" class="btn btn-danger">Sıfırla</button></div>
                            <div class="col-xs-6 text-right"><button type="button" class="btn btn-primary" id="DanismanKaydet">Kaydet</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
    var iletisimverisi = '<div class="form-group"><div class="col-xs-3 col-md-2"></div><div class="col-xs-3 col-md-2"><select class="select2" name="iletisimtipi[]"><?=$iletisimselect?></select></div><div class="col-xs-4 col-md-7"><input type="text" class="form-control" name="iletisim[]" /></div><div class="col-xs-2 col-md-1"><button type="button" class="btn btn-danger" onclick="$(this).closest(\'div.form-group\').remove()">Sil</button></div></div>';
    $(document).ready(function(){
        tinymce.init({
            selector: '#hakkinda',
            language: 'tr_TR',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true
        });

        $('#IletisimEkle').on('click',function () {
            $('#iletisimDiv').append(iletisimverisi);
            $('.select2').select2({width:'100%'});
        });

        $('.select2').select2({width:'100%'});

        $('#DanismanKaydet').on('click',function(){
            if($('#adi').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    icon:'fa fa-exclamation',
                    title:'Lütfen danışman adını boş bırakmayınız.',
                    content:''
                });
            }
            else if($('#soyadi').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    icon:'fa fa-exclamation',
                    title:'Lütfen danışman soyadını boş bırakmayınız.',
                    content:''
                });;
            }
            else{
                $.blockUI();
                $('#DanismanForm').submit();
            }
        });
    });
</script>