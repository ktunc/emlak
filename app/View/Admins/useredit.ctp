<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><em><?=$user['User']['username']?></em> Kullanıcısını Düzenle</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" role="form" class="form-horizontal" id="UserForm" enctype="multipart/form-data">
                    <input type="hidden" name="user_id" value="<?=$user['User']['id']?>"/>
                    <div class="form-group">
                        <label for="name" class="col-xs-3 col-md-2 control-label">Adı:</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="name" id="name" placeholder="Adı" class="form-control" value="<?=$user['User']['name']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-xs-3 col-md-2 control-label">Kullanıcı Adı:</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="username" id="username" placeholder="Kullanıcı Adı" class="form-control" disabled="disabled" value="<?=$user['User']['username']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-xs-3 col-md-2 control-label">Kullanıcı Mail:</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="email" id="email" placeholder="Kullanıcı Mail" class="form-control" disabled="disabled" value="<?=$user['User']['email']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass" class="col-xs-3 col-md-2 control-label">Kullanıcı Şifre:</label>
                        <div class="col-xs-9 col-md-10">
                            <input type="text" name="pass" id="pass" placeholder="Kullanıcı Şifre" class="form-control" value="<?=$user['User']['keke']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group" class="col-xs-3 col-md-2 control-label">Kullanıcı Grubu:</label>
                        <div class="col-xs-9 col-md-10">
                            <select class="select2" name="group">
                                <option value="1" <?=$user['User']['group']==1?'selected':''?>>Admin</option>
                                <option value="2" <?=$user['User']['group']==2?'selected':''?>>Kullanıcı</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="durum" class="col-xs-3 col-md-2 control-label">Kullanıcı Durumu:</label>
                        <div class="col-xs-9 col-md-10">
                            <select class="select2" name="durum">
                                <option value="1" <?=$user['User']['durum']==1?'selected':''?>>Aktif</option>
                                <option value="0" <?=$user['User']['durum']==0?'selected':''?>>Pasif</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <hr>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-sm-offset-2">
                            <div class="col-xs-6"><a href="<?=$this->Html->url('/')?>admins/userlar" class="btn btn-danger">İptal</a></div>
                            <div class="col-xs-6 text-right"><button type="button" class="btn btn-primary" id="UserKaydet">Kaydet</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.select2').select2({width:'100%'});

        $('#UserKaydet').on('click',function(){
            if($('#name').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    icon:'fa fa-exclamation',
                    title:'Lütfen \'Adı\' alanını boş bırakmayınız.',
                    content:''
                });
            }
            else if($('#username').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    icon:'fa fa-exclamation',
                    title:'Lütfen \'Kullanıcı Adı\' alanını boş bırakmayınız.',
                    content:''
                });
            }
            else if($('#email').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    icon:'fa fa-exclamation',
                    title:'Lütfen \'Kullanıcı Mail\' alanını boş bırakmayınız.',
                    content:''
                });
            }
            else if($('#pass').val().length < 6){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    icon:'fa fa-exclamation',
                    title:'Lütfen \'Kullanıcı Şifre\' alanı en az 6 karakter olmalıdır.',
                    content:''
                });
            }
            else{
                var formdata = new FormData($('form#UserForm').get(0));
                $.ajax({
                    type:'POST',
                    url:'<?=$this->Html->url('/')?>admins/userkaydet',
                    data:formdata,
                    beforeSend:function () {
                        $.blockUI();
                    },
                    processData: false,
                    contentType: false
                }).done(function (data) {
                    var dat = $.parseJSON(data);
                    if(dat['hata']){
                        $.confirm({
                            theme:'modern',
                            type:'red',
                            icon:'fa fa-close',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        if(dat['link']){
                                            window.location.href = dat['link'];
                                        }
                                    }
                                }
                            }
                        });
                    }else{
                        $.confirm({
                            theme:'modern',
                            type:'green',
                            icon:'fa fa-check',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        window.location.href = dat['link'];
                                    }
                                }
                            }
                        });
                    }
                }).fail(function () {
                    $.alert({
                        theme:'modern',
                        type:'red',
                        icon:'fa fa-close',
                        title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                        content:'',
                        onContentReady:function () {
                            $.unblockUI();
                        }
                    });
                });
            }
        });
    });
</script>