<?php
echo $this->Html->css('jquery.filer');
echo $this->Html->css('themes/jquery.filer-dragdropbox-theme');
echo $this->Html->script('jquery.filer.min');
echo $this->Html->script('jquery.filer.custom');
echo $this->Html->script('locationpicker.jquery');
echo $this->Html->script('http://maps.google.com/maps/api/js?key=AIzaSyBb6wy1FSr2ms69Cy7BSuZQLOB9-EPIkIA&sensor=false&libraries=places');
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo $projes['Proje']['id'];?> ID'li Proje Düzenle</h5>
            </div>
            <div class="ibox-content">
<div class="">
    <form  method="POST" role="form" class="form-horizontal" id="ProjeForm" enctype="multipart/form-data">
        <input type="hidden" name="proje_id" value="<?php echo $projes['Proje']['id'] ?>"/>
        <div class="form-group">
            <label for="baslik" class="col-xs-3 col-md-2 control-label">Başlık</label>
            <div class="col-xs-9 col-md-10">
                <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control" value="<?php echo $projes['Proje']['baslik'] ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="aciklama" class="col-xs-3 col-md-2 control-label">Açıklama</label>
            <div class="col-xs-9 col-md-10">
                <textarea name="aciklama" id="aciklama" placeholder="Açıklama" class="form-control" rows="10"><?php echo $projes['Proje']['aciklama']; ?></textarea>
            </div>
        </div>
        <!-- Resim -->
        <div class="form-group">
            <label for="resim" class="control-label col-xs-3 col-md-2 ">Resimler</label>
        </div>
        <div class="form-group">
            <?php
            foreach($projes['ProjeResim'] as $cow){
                ?>
                <div class="col-sm-6 col-md-3 resims" id="Res_<?php echo $cow['id'];?>">
                    <div class="thumbnail" style="background-color: #FFFFFF" >
                        <img src="<?php echo $this->Html->url('/').$cow['path'];?>" alt="" style="width:300px;height:200px;" alt="<?php echo $projes['Proje']['baslik'].' '.$cow['id']; ?>" />
                        <button type="button" class="btn btn3d btn-danger" onclick="DeleteResim('<?php echo $cow['id'];?>')">Sil</button>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="form-group">
            <hr>
        </div>
        <div class="form-group">
            <div id="content">
                <!-- Example 2 -->
                <input type="file" name="files[]" id="filer_input2" multiple="multiple" accept="image/*">
                <!-- end of Example 2 -->
            </div>
        </div>
        <div class="form-group">
            <hr>
        </div>
        <div class="form-group">
            <label for="us2-address" class="col-xs-3 col-md-2 control-label">Location</label>
            <div class="col-xs-9 col-md-10">
                <input type="text" id="us2-address" name="location"  class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12" id="us2" style="min-height: 400px;margin-top: 2%;"></div>
            <input type="hidden" id="us2-lat" name="latitude" />
            <input type="hidden" id="us2-lon" name="longitude" />
        </div>
        <div class="form-group">
            <hr>
        </div>
        <div class="form-group">
            <div class="col-xs-12" style="margin-top:5%">
                <button type="button" class="btn btn3d btn-primary" id="ProjeKaydet">Kaydet</button>
                <button type="button" class="btn btn3d btn-default" id="ProjeIptal">İptal</button>
                <button type="button" class="btn btn3d btn-danger" style="float:right" onclick="ProjeSil(<?php echo $projes['Proje']['id'];?>)">Sil</button>
            </div>
        </div>
    </form>
</div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        tinymce.init({
            selector: '#aciklama',
            language: 'tr_TR',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        });

        $('#ProjeKaydet').on('click',function(){
            if($('#baslik').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    columnClass:'l',
                    icon:'fa fa-exclamation',
                    title:"Lütfen 'Başlık' alanını boş bırakmayınız.",
                    content:''
                });
            }
            else{
                var formdata = new FormData($('form#ProjeForm').get(0));
                $.ajax({
                    type:'POST',
                    url:'<?php echo $this->Html->url('/');?>admins/projekaydet',
                    data:formdata,
                    beforeSend:function () {
                        $.blockUI();
                    },
                    processData: false,
                    contentType: false
                }).done(function (data) {
                    var dat = $.parseJSON(data);
                    if(dat['hata']){
                        $.confirm({
                            theme:'modern',
                            type:'red',
                            icon:'fa fa-close',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        if(dat['link']){
                                            window.location.href = dat['link'];
                                        }
                                    }
                                }
                            }
                        });
                    }else{
                        $.confirm({
                            theme:'modern',
                            type:'green',
                            icon:'fa fa-check',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        window.location.href = dat['link'];
                                    }
                                }
                            }
                        });
                    }
                }).fail(function () {
                    $.alert({
                        theme:'modern',
                        type:'red',
                        icon:'fa fa-close',
                        title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                        content:'',
                        onContentReady:function () {
                            $.unblockUI();
                        }
                    });
                });
            }
        });

        $('#us2').locationpicker({
        location: {latitude: <?php echo empty($projes['Proje']['latitude'])?0:$projes['Proje']['latitude'];?>, longitude: <?php echo empty($projes['Proje']['longitude'])?0:$projes['Proje']['longitude'];?>},
        radius: 10,
        inputBinding: {
            latitudeInput: $('#us2-lat'),
            longitudeInput: $('#us2-lon'),
            radiusInput: $('#us2-radius'),
            locationNameInput: $('#us2-address')
            },
            enableAutocomplete: true
        });

        $('#ProjeIptal').on('click',function(){
            window.location.href = '<?php echo $this->Html->url('/');?>admins/projeler';
        });
    });

    function DeleteResim(resId){
        $.confirm({
            type:'orange',
            theme:'modern',
            icon:'fa fa-question',
            title:'Resmi silmek istediğinizden emin misiniz?',
            content:'',
            buttons:{
                iptal:{
                    text:'İptal'
                },
                sil:{
                    text:'<i class="fa fa-trash"></i> Sil',
                    btnClass:'btn-danger',
                    action:function(){
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo $this->Html->url('/');?>admins/projedeleteresim",
                            data: 'resId='+resId,
                            beforeSend:function () {
                                $.blockUI();
                            }
                        }).done(function (data) {
                            var dat = $.parseJSON(data);
                            if(dat['hata']){
                                $.alert({
                                    theme:'modern',
                                    type:'red',
                                    icon:'fa fa-close',
                                    title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    }
                                });
                            }else{
                                $.alert({
                                    theme:'modern',
                                    type:'green',
                                    icon:'fa fa-check',
                                    title:'Resim başarıyla silindi.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    }
                                });
                                $('#Res_'+resId).remove();
                            }
                        }).fail(function () {
                            $.alert({
                                theme:'modern',
                                type:'red',
                                icon:'fa fa-close',
                                title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                content:'',
                                onContentReady:function(){
                                    $.unblockUI();
                                }
                            });
                        });
                    }
                }
            }
        });
    }

    function ProjeSil(projeId){
        $.confirm({
            type:'orange',
            theme:'modern',
            icon:'fa fa-question',
            title:'İlanı silmek istediğinizden emin misiniz?',
            content:'',
            buttons:{
                iptal:{
                    text:'İptal'
                },
                sil:{
                    text:'<i class="fa fa-trash"></i> Sil',
                    btnClass:'btn-danger',
                    action:function(){
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo $this->Html->url('/');?>admins/projesil",
                            data:"projeId="+projeId,
                            beforeSend:function () {
                                $.blockUI();
                            }
                        }).done(function (data) {
                            var dat = $.parseJSON(data);
                            if(dat['hata']){
                                $.alert({
                                    theme:'modern',
                                    type:'red',
                                    icon:'fa fa-close',
                                    title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    }
                                });
                            }else{
                                $.alert({
                                    theme:'modern',
                                    type:'green',
                                    icon:'fa fa-check',
                                    title:'İlan başarıyla silindi.',
                                    content:'',
                                    onContentReady:function(){
                                        $.unblockUI();
                                    },
                                    onClose:function(){
                                        window.location.href = "<?=$this->Html->url('/')?>admins/projeler";
                                    }
                                });
                            }
                        }).fail(function () {
                            $.alert({
                                theme:'modern',
                                type:'red',
                                icon:'fa fa-close',
                                title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                content:'',
                                onContentReady:function(){
                                    $.unblockUI();
                                }
                            });
                        });
                    }
                }
            }
        });
    }
</script>