<?=$this->Html->css('admin/plugins/dataTables/datatables.min');?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>İş Yeri İlanları</h5>

            </div>
            <div class="ibox-content">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="5%">İlan No</th>
                            <th>Başlık</th>
                            <th width="5%">Tarih</th>
                            <th width="5%">Satılık-Kiralık</th>
                            <th width="5%">Durum</th>
                            <th width="5%">Düzenle</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($isyeriler as $row){
                            echo '<tr>';
                            echo '<td>'.$row['Isyeri']['id'].'</td>';
                            echo '<td>'.$row['Isyeri']['ilan_no'].'</td>';
                            echo '<td class="tooltipss" title="'.$row['Isyeri']['baslik'].'">'.CakeText::truncate($row['Isyeri']['baslik'],25).'</td>';
                            echo '<td>'.date('d.m.Y',strtotime($row['Isyeri']['tarih'])).'</td>';
                            if($row['Isyeri']['sat_kir'] == 1){
                                echo '<td>Satılık</td>';
                            }else{
                                echo '<td>Kiralık</td>';
                            }
                            if($row['Isyeri']['durum'] == 1){
                                echo '<td class="text-center tooltipss"><i class="fa fa-lg fa-check text-info tooltipss" title="Yayinda" data-kid="'.$row['Isyeri']['id'].'"></i></td>';
                            }else{
                                echo '<td class="text-center tooltipss"><i class="fa fa-lg fa-close text-danger tooltipss" title="Yayindan Kalktı" data-kid="'.$row['Isyeri']['id'].'"></i></td>';
                            }
                            echo '<td class="text-center"><i class="fa fa-lg fa-edit text-warning tooltipss" title="Düzenle" onclick="window.location.href=\''.$this->Html->url('/').'admins/isyeriedit?id='.$row['Isyeri']['id'].'\'"></i></td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>İlan No</th>
                            <th>Başlık</th>
                            <th>Tarih</th>
                            <th>Satılık-Kiralık</th>
                            <th>Durum</th>
                            <th>Düzenle</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('admin/plugins/dataTables/datatables.min')?>
<script type="text/javascript">
$(document).ready(function(){
    $('.dataTables-example').DataTable({
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]

    });
});
</script>
