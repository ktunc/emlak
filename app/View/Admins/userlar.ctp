<?=$this->Html->css('admin/plugins/dataTables/datatables.min');?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Kullanıcılar</h5>
            </div>
            <div class="ibox-content">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th>Kullanıcı</th>
                            <th>Kullanıcı Adı</th>
                            <th>Kullanıcı Email</th>
                            <th>Kullanıcı Grup</th>
                            <th width="5%">Tarih</th>
                            <th width="5%">Durum</th>
                            <th width="5%">Düzenle</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($users as $row){
                            echo '<tr>';
                            echo '<td>'.$row['User']['id'].'</td>';
                            echo '<td>'.$row['User']['name'].'</td>';
                            echo '<td>'.$row['User']['username'].'</td>';
                            echo '<td>'.$row['User']['email'].'</td>';
                            if($row['User']['group'] == 1){
                                echo '<td>Admin</td>';
                            }else{
                                echo '<td>Kullanıcı</td>';
                            }
                            echo '<td>'.date('d.m.Y H:i:s',strtotime($row['User']['tarih'])).'</td>';

                            if($row['User']['durum'] == 1){
                                echo '<td class="text-center tooltipss"><i class="fa fa-lg fa-check text-info tooltipss" title="Aktif" data-kid="'.$row['User']['id'].'"></i></td>';
                            }else{
                                echo '<td class="text-center tooltipss"><i class="fa fa-lg fa-close text-danger tooltipss" title="Pasif" data-kid="'.$row['User']['id'].'"></i></td>';
                            }
                            echo '<td class="text-center"><a href="'.$this->Html->url('/').'admins/useredit?user_id='.$row['User']['id'].'"><i class="fa fa-lg fa-edit text-warning tooltipss" title="Düzenle"></i></a></td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th width="5%">ID</th>
                            <th>Kullanıcı</th>
                            <th>Kullanıcı Adı</th>
                            <th>Kullanıcı Email</th>
                            <th>Kullanıcı Grup</th>
                            <th width="5%">Tarih</th>
                            <th width="5%">Durum</th>
                            <th width="5%">Düzenle</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('admin/plugins/dataTables/datatables.min')?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });
    });
</script>
