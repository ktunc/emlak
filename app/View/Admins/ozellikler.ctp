<?=$this->Html->css('admin/plugins/dataTables/datatables.min');?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>İlan Özellikleri</h5>
                <div class="ibox-tools">
                    <button type="button" class="btn btn-success" onclick="FuncOzellikYeni()"><i class="fa fa-plus"></i> Yeni Özellik Ekle</button>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th>Özellik</th>
                            <th width="5%">Tarih</th>
                            <th width="5%">Düzenle</th>
                            <th width="5%">Sil</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($ozellikler as $row){
                            echo '<tr>';
                            echo '<td>'.$row['Ozellik']['id'].'</td>';
                            echo '<td>'.$row['Ozellik']['ozellik'].'</td>';
                            echo '<td>'.date('d.m.Y',strtotime($row['Ozellik']['tarih'])).'</td>';
                            echo '<td class="text-center"><i class="fa fa-lg fa-edit text-warning tooltipss" title="Düzenle" onclick="FuncOzellikDuzenle('.$row['Ozellik']['id'].')"></i></td>';
                            echo '<td class="text-center"><i class="fa fa-lg fa-trash text-danger tooltipss" title="Düzenle" onclick="FuncOzellikSil('.$row['Ozellik']['id'].')"></i></td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Özellik</th>
                            <th>Tarih</th>
                            <th>Düzenle</th>
                            <th>Sil</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('admin/plugins/dataTables/datatables.min')?>
<script type="text/javascript">
    var jc;
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });
    });

    function FuncOzellikDuzenle(oId) {
        $.ajax({
            type:'POST',
            url:'<?=$this->Html->url('/')?>admins/ozellikgetir',
            data:{'oId':oId},
            beforeSend:function () {
                $.blockUI();
            }
        }).done(function (data) {
            var dat = $.parseJSON(data);
            if(dat['hata']){
                $.alert({
                    theme:'modern',
                    type:'red',
                    icon:'fa fa-close',
                    title:'Bir hata meydana geldi. Lütfen tekrar deneyiniz.',
                    content:'',
                    onContentReady:function () {
                        $.unblockUI();
                    }
                });
            }else{
                jc = $.dialog({
                    closeIcon:false,
                    columnClass:'l',
                    theme:'modern',
                    type:'blue',
                    title:'Yeni İlan Özelliği',
                    content:'<div class="col-xs-12"><form method="POST" role="form" class="form-horizontal" id="OzellikForm" enctype="multipart/form-data"><input type="hidden" name="ozellik_id" value="'+dat['ozellik_id']+'"/><div class="form-group"><label for="ozellik" class="col-xs-3 col-md-2 control-label">Özellik:</label><div class="col-xs-9 col-md-10"><input type="text" name="ozellik" id="ozellik" placeholder="Özellik" class="form-control" value="'+dat['ozellik']+'"/></div></div><div class="form-group"><div class="col-xs-6"><button type="button" class="btn btn-default" onclick="jc.close()">Vazgeç</button></div><div class="col-xs-6"><button type="button" class="btn btn-primary pull-right" onclick="FuncOzellikKaydet()">Kaydet</button></div></div></form></div>',
                    onContentReady:function () {
                        $.unblockUI();
                    }
                });
            }
        }).fail(function () {
            $.alert({
                theme:'modern',
                type:'red',
                icon:'fa fa-close',
                title:'Bir hata meydana geldi. Lütfen tekrar deneyiniz.',
                content:'',
                onContentReady:function () {
                    $.unblockUI();
                }
            });
        });
    }

    function FuncOzellikYeni() {
        jc = $.dialog({
            closeIcon:false,
            columnClass:'l',
            theme:'modern',
            type:'blue',
            title:'Yeni İlan Özelliği',
            content:'<div class="col-xs-12"><form method="POST" role="form" class="form-horizontal" id="OzellikForm" enctype="multipart/form-data"><input type="hidden" name="ozellik_id" value="0"/><div class="form-group"><label for="ozellik" class="col-xs-3 col-md-2 control-label">Özellik:</label><div class="col-xs-9 col-md-10"><input type="text" name="ozellik" id="ozellik" placeholder="Özellik" class="form-control" /></div></div><div class="form-group"><div class="col-xs-6"><button type="button" class="btn btn-default" onclick="jc.close()">Vazgeç</button></div><div class="col-xs-6"><button type="button" class="btn btn-primary pull-right" onclick="FuncOzellikKaydet()">Kaydet</button></div></div></form></div>'
        });
    }

    function FuncOzellikKaydet() {
        if($('form#OzellikForm input[name="ozellik"]').val() == ""){
            $.alert({
                theme:'modern',
                type:'orange',
                icon:'fa fa-excalamation',
                title:'Lütfen özellik giriniz.',
                content:''
            });
        }else{
            var formdata = new FormData($('form#OzellikForm').get(0));
            $.ajax({
                type:'POST',
                url:'<?=$this->Html->url('/')?>admins/ozellikkaydet',
                data:formdata,
                beforeSend:function () {
                    jc.showLoading();
                },
                processData: false,
                contentType: false
            }).done(function (data) {
                var dat = $.parseJSON(data);
                if(dat['hata']){
                    $.alert({
                        theme:'modern',
                        type:'red',
                        icon:'fa fa-close',
                        title:'Bir hata meydana geldi. Lütfen tekrar deneyiniz.',
                        content:'',
                        onContentReady:function () {
                            jc.hideLoading();
                        }
                    });
                }else{
                    $.alert({
                        theme:'modern',
                        type:'green',
                        icon:'fa fa-check',
                        title:'Özellik başarıyla kaydedildi.',
                        content:'',
                        buttons:{
                            tamam:{
                                text:'Tamam',
                                action:function () {
                                    window.location.reload();
                                }
                            }
                        }
                    });
                }
            }).fail(function () {
                $.alert({
                    theme:'modern',
                    type:'red',
                    icon:'fa fa-close',
                    title:'Bir hata meydana geldi. Lütfen tekrar deneyiniz.',
                    content:'',
                    onContentReady:function () {
                        jc.hideLoading();
                    }
                });
            });
        }
    }

    function FuncOzellikSil(oId) {
        $.confirm({
            closeIcon:false,
            columnClass:'l',
            theme:'modern',
            type:'orange',
            icon:'fa fa-exclamation',
            title:'Özelliği Silmek İstediğinizden Emin Misiniz?',
            content:'',
            buttons:{
                vazgec:{
                    text:'Vazgeç'
                },
                sil:{
                    text:'<i class="fa fa-trash"></i> Sil',
                    btnClass:'btn-danger',
                    action:function () {
                        FuncOzellikSSil(oId);
                    }
                }
            }
        })
    }

    function FuncOzellikSSil(oId) {
        $.ajax({
            type:'POST',
            url:'<?=$this->Html->url('/')?>admins/ozelliksil',
            data:{'oId':oId},
            beforeSend:function () {
                $.blockUI();
            }
        }).done(function (data) {
            var dat = $.parseJSON(data);
            if(dat['hata']){
                $.alert({
                    theme:'modern',
                    type:'red',
                    icon:'fa fa-close',
                    title:'Bir hata meydana geldi. Lütfen tekrar deneyiniz.',
                    content:'',
                    onContentReady:function () {
                        $.unblockUI();
                    }
                });
            }else{
                $.alert({
                    theme:'modern',
                    type:'green',
                    icon:'fa fa-check',
                    title:'Özellik başarıyla silindi',
                    content:'',
                    onClose:function () {
                        window.location.reload();
                    }
                });
            }
        }).fail(function () {
            $.alert({
                theme:'modern',
                type:'red',
                icon:'fa fa-close',
                title:'Bir hata meydana geldi. Lütfen tekrar deneyiniz.',
                content:'',
                onContentReady:function () {
                    $.unblockUI();
                }
            });
        });
    }
</script>
