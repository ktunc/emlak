<?=$this->Html->css('admin/plugins/dataTables/datatables.min');?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Danışmanlar</h5>

            </div>
            <div class="ibox-content">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th>Adı</th>
                            <th>Soyadı</th>
                            <th width="5%">İlanları</th>
                            <th width="5%">Durum</th>
                            <th width="5%">Düzenle</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($danismanlar as $row){
                            echo '<tr>';
                            echo '<td>'.$row['Danisman']['id'].'</td>';
                            echo '<td>'.$row['Danisman']['adi'].'</td>';
                            echo '<td>'.$row['Danisman']['soyadi'].'</td>';
                            echo '<td class="text-center"><a href="'.$this->Html->url('/').'admins/ilanlar?danisman_id='.$row['Danisman']['id'].'" target="_blank"><i class="fa fa-link tooltipss" title="Danışmanın İlanlarını Görüntüle"></i></a></td>';
                            if($row['Danisman']['durum'] == 1){
                                echo '<td class="text-center"><i class="fa fa-lg fa-check text-info tooltipss" title="Yayinda" data-kid="'.$row['Danisman']['id'].'"></i></td>';
                            }else{
                                echo '<td class="text-center"><i class="fa fa-lg fa-close text-danger tooltipss" title="Yayindan Kalktı" data-kid="'.$row['Danisman']['id'].'"></i></td>';
                            }
                            echo '<td class="text-center"><a href="'.$this->Html->url('/').'admins/danismanedit?id='.$row['Danisman']['id'].'" target="_blank"><i class="fa fa-lg fa-edit text-warning tooltipss" title="Düzenle"></i></a></td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th width="5%">ID</th>
                            <th>Adı</th>
                            <th>Soyadı</th>
                            <th width="5%">İlanları</th>
                            <th width="5%">Durum</th>
                            <th width="5%">Düzenle</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('admin/plugins/dataTables/datatables.min')?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });
    });
</script>
