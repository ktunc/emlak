<?php
echo $this->Html->css('jquery.filer');
echo $this->Html->css('themes/jquery.filer-dragdropbox-theme');
echo $this->Html->script('locationpicker.jquery');
echo $this->Html->script('http://maps.google.com/maps/api/js?key=AIzaSyBb6wy1FSr2ms69Cy7BSuZQLOB9-EPIkIA&sensor=false&libraries=places');
echo $this->Html->script('jquery.filer.min');
echo $this->Html->script('jquery.filer.custom');

$sehir = '<option value="0">Seçiniz</option>';
foreach ($il as $row) {
    $sehir .= '<option value="'.$row['Sehir']['id'].'">'.$row['Sehir']['sehir_adi'].'</option>';
}

//$iletSel = '<div class="anaDiv"><div class="div85">';
//$iletSel .= '<select name="ilanilet[]" class="ilanilet form-control"><option value="0">Seçiniz</option>';
//foreach($iletisim as $cow){
//    $iletSel .= '<option value="'.$cow['IlanIletisim']['id'].'">';
//    if($cow['IlanIletisim']['type'] == 1){
//        $iletSel .= 'Telefon: ';
//    }else if($cow['IlanIletisim']['type'] == 2){
//        $iletSel .= 'Faks: ';
//    }else if($cow['IlanIletisim']['type'] == 3){
//        $iletSel .= 'Email: ';
//    }else if($cow['IlanIletisim']['type'] == 4){
//        $iletSel .= 'Web Sitesi: ';
//    }else if($cow['IlanIletisim']['type'] == 5){
//        $iletSel .= 'Adres: ';
//    }
//    $iletSel .= $cow['IlanIletisim']['iletisim'].'</option>';
//}
//$iletSel .= '</select></div><div class="div15 text-left"><button type="button" class="btn btn-sm btn-danger iletSil"><i class="fa fa-minus"></i> Sil</button></div></div>';

$danismanSelect = '<option value="0">Seçiniz</option>';
foreach ($danismanlar as $row){
    $danismanSelect .= '<option value="'.$row['Danisman']['id'].'">'.$row['Danisman']['adi'].' '.$row['Danisman']['soyadi'].'</option>';
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Yeni Konut</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" role="form" class="form-horizontal" action="<?php echo $this->Html->url('/');?>admins/konutadd" id="KonutForm" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="baslik" class="col-xs-3 col-md-2 control-label">Başlık</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="aciklama" class="col-xs-3 col-md-2 control-label">Açıklama</label>
                    <div class="col-xs-9 col-md-10">
                        <textarea name="aciklama" rows="5" id="aciklama" placeholder="Açıklama" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="il" class="col-xs-3 col-md-2 control-label">Şehir</label>
                    <div class="col-xs-9 col-md-10">
                        <select id="il" name="il" class="form-control"><?php echo $sehir; ?></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ilce" class="col-xs-3 col-md-2 control-label">İlçe</label>
                    <div class="col-xs-9 col-md-10">
                        <select id="ilce" name="ilce" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="semt" class="col-xs-3 col-md-2 control-label">Semt</label>
                    <div class="col-xs-9 col-md-10">
                        <select id="semt" name="semt" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="mahalle" class="col-xs-3 col-md-2 control-label">Mahalle</label>
                    <div class="col-xs-9 col-md-10">
                        <select id="mahalle" name="mahalle" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="oda" class="col-xs-3 col-md-2 control-label">Oda Sayısı</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="text" name="oda" id="oda" placeholder="Oda Sayısı" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="m2" class="col-xs-3 col-md-2 control-label">Metre Kar</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="number" name="m2" id="m2" placeholder="Metre Kare" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kat" class="col-xs-3 col-md-2 control-label">Kaçıncı Katta</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="text" name="kat" id="kat" placeholder="Kaçıncı Katta" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="binakat" class="col-xs-3 col-md-2 control-label">Bina Katı</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="number" name="binakat" id="binakat" placeholder="Bina Katı" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kredi" class="col-xs-3 col-md-2 control-label">Kredi Uygunluğu</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="text" name="kredi" id="kredi" placeholder="Kredi Uygunluğu" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fiyat" class="col-xs-3 col-md-2 control-label">Fiyat</label>
                    <div class="col-xs-9 col-md-10">
                        <div class="input-group">
                        <span class="input-group-addon" style="padding-top:0;padding-bottom:0;">
                            <select name="paraBirimi"><option value="TL">TL</option><option value="$">$</option><option value="€">€</option></select>
                        </span>
                            <input type="number" name="fiyat" id="fiyat" placeholder="Fiyat" class="form-control"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="sat_kir" class="col-xs-3 col-md-2 control-label">Satılık - Kiralık</label>
                    <div class="col-xs-9 col-md-10">
                        <select id="sat_kir" name="sat_kir" class="form-control">
                            <option value="1">Satılık</option>
                            <option value="2">Kiralık</option>
                        </select>
                    </div>
                </div>
                <!-- Mal Sahibi Bilgileri -->
                <div class="form-group">
                    <label for="malName" class="col-xs-3 col-md-2 control-label">Mal Sahibi Adı Soyadı</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="text" name="malName" id="malName" placeholder="Mal Sahibi Adı Soyadı" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="malTelNo" class="col-xs-3 col-md-2 control-label">Mal Sahibi Telefon No:</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="text" name="malTelNo" id="malTelNo" placeholder="Mal Sahibi Telefon No" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="malFiyat" class="col-xs-3 col-md-2 control-label">Mal Sahibi Fiyat</label>
                    <div class="col-xs-9 col-md-10">
                        <div class="input-group">
                        <span class="input-group-addon" style="padding-top:0;padding-bottom:0;">
                            <select name="malParaBirimi"><option value="TL">TL</option><option value="$">$</option><option value="€">€</option></select>
                        </span>
                            <input type="number" name="malFiyat" id="malFiyat" placeholder="Mal Sahibi Fiyat" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="resim" class="control-label col-xs-3 col-md-2 ">Danışmanlar</label>
                    <div class="col-xs-9 col-md-10">
                        <table id="danismanTable" class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Danışman</th>
                                <th>Sil</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-3 col-md-offset-2"><button type="button" class="btn btn-xs btn-success" id="DanismanEkle"><i class="fa fa-plus"></i> Danışman Ekle</button></div>
                </div>
                <div class="form-group"><hr></div>
                <!-- Mal Sahibi Bilgileri SON -->
                <!-- İlan İletişim Bilgileri -->
                <!--
                <div class="form-group">
                    <h4>İlan İletişimi</h4>
                </div>
                <div class="form-group">
                    <div id="ilanDiv">

                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-primary" id="IletEkle"><i class="fa fa-plus"></i> İletişim Ekle</button>
                </div>
                <div class="form-group">
                    <hr>
                </div>
                -->
                <!-- İlan İletişim Bilgileri SON -->
                <div class="form-group">
                    <label for="resim control-label">Resimler</label>
                </div>
                <div class="form-group">
                    <div id="content">
                        <!-- Example 2 -->
                        <input type="file" name="files[]" id="filer_input2" multiple="multiple" accept="image/*">
                        <!-- end of Example 2 -->
                    </div>
                </div>

                <div class="form-group">
                    <hr>
                </div>
                <div class="form-group">
                    <label for="us2-address" class="col-xs-3 col-md-2 control-label">Location</label>
                    <div class="col-xs-9 col-md-10">
                        <input type="text" id="us2-address" name="location"  class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12" id="us2" style="min-height: 400px;margin-top: 2%;"></div>
                    <input type="hidden" id="us2-lat" name="latitude" />
                    <input type="hidden" id="us2-lon" name="longitude" />
                </div>
                <div class="form-group">
                    <div class="col-xs-12" style="margin-top:5%">
                        <button type="button" class="btn btn-sm btn-primary" id="KonutKaydet"><i class="fa fa-plus"></i> Kaydet</button>
                        <button type="reset" class="btn btn-sm btn-danger" id="KonutReset">Sıfırla</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- MODALLAR **********************************************************************************************-->
<!-- Loader ***********************************************-->
<div id="Loader" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
    </div>
</div>
<!-- Loader ***********************************************-->
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
    var danisman = '<tr><td><select name="danisman[]"><?=$danismanSelect?></select></td><td><button type="button" class="btn btn-xs btn-danger danismanSil">Sil</button></td></tr>';
    $(document).ready(function(){
        tinymce.init({
            selector: '#aciklama',
            language: 'tr_TR',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true
        });

        $('#il,#ilce,#semt,#mahalle,.ilanilet').select2({
            width:'100%'
        });
        $('[data-toggle="tooltip"]').tooltip();

        $('#DanismanEkle').on('click',function () {
            $('table#danismanTable tbody').append(danisman);
        });

        $('table#danismanTable tbody').on('click','.danismanSil',function () {
            $(this).closest('tr').remove();
        });

        $('#il').on('change',function (){
            $.blockUI();
            $('#ilce').html('');
            $('#semt').html('');
            var il = $(this).val();
            if(il != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getIlce",
                    data: 'il='+il,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Ilce']['id']+'">'+vall['Ilce']['ilce_adi']+'</option>';
                            });
                            $('#ilce').html(ekle);
                            $.unblockUI();
                        }
                    }
                });
            }else{
                $.unblockUI();
            }
        });

        $('#ilce').on('change',function (){
            $.blockUI();
            $('#semt').html('');
            $('#mahalle').html('');
            var ilce = $(this).val();
            if(ilce != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getSemt",
                    data: 'ilce='+ilce,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Semt']['id']+'">'+vall['Semt']['semt_adi']+'</option>';
                            });
                            $('#semt').html(ekle);
                            $.unblockUI();
                        }
                    }
                });
            }else{
                $.unblockUI();
            }
        });

        $('#semt').on('change',function (){
            $.blockUI();
            $('#mahalle').html('');
            var semt = $(this).val();
            if(semt != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getMahalle",
                    data: 'semt='+semt,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Mahalle']['id']+'">'+vall['Mahalle']['mahalle_adi']+'</option>';
                            });
                            $('#mahalle').html(ekle);
                            $.unblockUI();
                        }
                    }
                });
            }else{
                $.unblockUI();
            }
        });

        $('#KonutKaydet').on('click',function(){
            if($('#baslik').val() == ''){
                $('#UyariModal #UyariContent').html('Başlık boş bırakılamaz.');
                $('#UyariModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                return false;
            }else if($('#il').val() == 0){
                $('#UyariModal #UyariContent').html('Lütfen şehir seçiniz.');
                $('#UyariModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                return false;
            } else{
                $.blockUI();
                $('#KonutForm').submit();
            }
        });

        $('#us2').locationpicker({
            location: {latitude: 39.918012967883385, longitude: 32.85808648203124},
            radius: 10,
            inputBinding: {
                latitudeInput: $('#us2-lat'),
                longitudeInput: $('#us2-lon'),
                radiusInput: $('#us2-radius'),
                locationNameInput: $('#us2-address')
            },
            enableAutocomplete: true
        });
    });

</script>