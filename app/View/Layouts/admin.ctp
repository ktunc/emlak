<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html lang="tr-TR">
<head>
    <?php echo $this->Html->charset();
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

    <meta name="description" content="Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı..." />
    <meta name="keywords" content="Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB, kiralık fabrika, Anadolu OSB, ASO 2 OSB, Konutkent, Çayyolu, Kuleevo, Elmar Towers, Elya Tower, Azel Kule" />
    <!-- <meta name="robots" content="index, follow" /> -->
    <meta name="rating" content="All" />
    <!--<meta name="copyright" content="www.batikapigayrimenkul.com" />
    <meta name="author" content="Soner HAYIRLI" />
	<meta name = "alexaVerifyID" content = " SiGSWe5sSp2pqHrlPdwbGEUXRzk " />-->
    <?php echo HtmlMetaComponent::elements(); ?>

    <title>
        <?php
        echo Configure::read('Defines.SiteName');
        ?>
    </title>
    <link rel="icon" type="image/x-icon" href="<?php echo $this->Html->url('/');?>img/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->Html->url('/');?>img/logo.png">
    <link hreflang="tr">
    <?php
    echo $this->Html->css(array(
        'admin/bootstrap.min',
        'admin/style',
        'admin/animate',
        'admin/plugins/tooltipster/tooltipster.bundle.min',
        'admin/plugins/tooltipster/themes/tooltipster-sideTip-punk.min',
        'font-awesome.min',
        'select2.min',
        'jquery-confirm.min',
        'admin/plugins/iCheck/all'
    ));
    echo $this->Html->script(array(
        'admin/jquery-3.1.1.min',
        'admin/bootstrap.min',
        'admin/plugins/metisMenu/jquery.metisMenu',
        'admin/plugins/slimscroll/jquery.slimscroll.min',
        'admin/inspinia',
        'admin/plugins/pace/pace.min',
        'select2/select2.full.min',
        'admin/plugins/tooltipster/tooltipster.bundle.min',
        'jquery-confirm.min',
        'blockUI',
        'admin/plugins/iCheck/icheck.min'
    ));
    ?>
    <!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/tr_TR/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Google Plus -->
<script src="https://apis.google.com/js/platform.js" async defer>
    //  {
    //      lang: 'tr'
    //  }
</script>

<!-- Twitter -->
<script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>

<div id="wrapper">

    <?=$this->element('adminmenu');?>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.7.1/search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Maden Gayrimenkul</span>
                    </li>
                    <li>
                        <a href="login.html">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

<!--        <div class="row wrapper border-bottom white-bg page-heading">-->
<!--            <div class="col-sm-4">-->
<!--                <h2>This is main title</h2>-->
<!--                <ol class="breadcrumb">-->
<!--                    <li>-->
<!--                        <a href="index-2.html">This is</a>-->
<!--                    </li>-->
<!--                    <li class="active">-->
<!--                        <strong>Breadcrumb</strong>-->
<!--                    </li>-->
<!--                </ol>-->
<!--            </div>-->
<!--            <div class="col-sm-8">-->
<!--                <div class="title-action">-->
<!--                    <a href="#" class="btn btn-primary">This is action area</a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <?php echo $this->Session->flash(); ?>

        <div class="wrapper wrapper-content animated fadeInRight">
            <?php echo $this->fetch('content'); ?>
        </div>
        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2017
            </div>
        </div>

    </div>
</div>
<?php echo $this->element('sql_dump'); ?>
<script type="text/javascript">
$(document).ready(function(){
    $('.tooltipss').tooltipster({
        theme: 'tooltipster-punk',
        contentAsHTML: true,
        animation:'grow'
    });
});
</script>
</body>
</html>