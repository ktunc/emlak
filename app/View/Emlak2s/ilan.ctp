<section class="section-dark no-padding">
    <!-- Slider main container -->
    <div id="swiper-gallery" class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php
            $i = 0;
            foreach ($ilan['IlanResim'] as $row){
                $i++;
            ?>
                <div class="swiper-slide">
                    <div class="slide-bg swiper-lazy" data-background="<?=$this->Html->url('/').$row['path']?>" data-sub-html="<strong>this is a caption 1</strong><br/>Second line of the caption"></div>
                    <!-- Preloader image -->
                    <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-9 col-lg-8 slide-desc-col animated fadeInUp gallery-slide-desc-<?=$i?>">
                                <div class="gallery-slide-cont">
                                    <div class="gallery-slide-cont-inner">
                                        <div class="gallery-slide-title pull-right">
                                            <h5 class="subtitle-margin">apartments for sale</h5>
                                            <h3><?=$ilan['Ilan']['baslik']?><span class="special-color">.</span></h3>
                                        </div>
                                        <div class="gallery-slide-estate pull-right hidden-xs">
                                            <i class="fa fa-home"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="gallery-slide-desc-price pull-right">
                                        <i class="fa fa-try"></i> <?=number_format($ilan['Ilan']['fiyat'],0,'','.')?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            <?php
            }
            ?>
        </div>

        <div class="slide-buttons slide-buttons-center">
            <a href="#" class="navigation-box navigation-box-next slide-next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
            <div id="slide-more-cont"></div>
            <a href="#" class="navigation-box navigation-box-prev slide-prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
        </div>

    </div>

</section>

<section class="thumbs-slider section-both-shadow">
    <div class="container">
        <div class="row">
            <div class="col-xs-1">
                <a href="#" class="thumb-box thumb-prev pull-left"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
            </div>
            <div class="col-xs-10">
                <!-- Slider main container -->
                <div id="swiper-thumbs" class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php
                        foreach ($ilan['IlanResim'] as $row) {
                        ?>
                            <div class="swiper-slide">
                                <img class="slide-thumb" src="<?=$this->Html->url('/').$row['paththumb']?>" alt="">
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-1">
                <a href="#" class="thumb-box thumb-next pull-right"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
            </div>
        </div>
    </div>
</section>

<section class="section-light no-bottom-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div class="details-image pull-left hidden-xs">
                            <i class="fa fa-home"></i>
                        </div>
                        <div class="details-title pull-left">
<!--                            <h5 class="subtitle-margin">apartment for sale</h5>-->
                            <h3><?=$ilan['Ilan']['baslik']?><span class="special-color">.</span></h3>
                        </div>
                        <div class="clearfix"></div>
                        <div class="title-separator-primary"></div>
                        <p class="details-desc"><?=$ilan['Ilan']['aciklama']?></p>
                        <div class="row margin-top-45">
                            <?php
                            $say = 0;
                            foreach ($ilan['IlanOzellik'] as $row){
                                if($say%4 == 0){
                                    echo '<div class="col-xs-6 col-sm-4">';
                                    echo '<ul class="details-ticks">';
                                }

                                echo '<li><i class="jfont">&#xe815;</i>'.$row['Ozellik']['ozellik'].'</li>';

                                $say++;

                                if($say%4 == 0 || count($ilan['IlanOzellik']) == $say){
                                    echo '</ul>';
                                    echo '</div>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
                        <div class="details-parameters-price"><?=$ilan['Ilan']['ilan_no']?></div>
                        <div class="details-parameters-price"><i class="fa fa-try"></i> <?=number_format($ilan['Ilan']['fiyat'],0,'','.')?></div>
                        <?php
                        if($ilan['Ilan']['ilan_tipi'] == 1){
                            echo $this->element('IlanBilgi/IlanBilgiSagKonut', array('data'=>$ilan['KonutBilgi']));
                        }else if($ilan['Ilan']['ilan_tipi'] == 2){
                            echo $this->element('IlanBilgi/IlanBilgiSagIsyeri', array('data'=>$ilan['IsyeriBilgi']));
                        }else if($ilan['Ilan']['ilan_tipi'] == 3){
                            echo $this->element('IlanBilgi/IlanBilgiSagArsa', array('data'=>$ilan['ArsaBilgi']));
                        }
                        ?>
                    </div>
                </div>

<!--                <div class="row margin-top-45">-->
<!--                    --><?php
//                    $say = 0;
//                    foreach ($ilan['IlanOzellik'] as $row){
//                        if($say%4 == 0){
//                            echo '<div class="col-xs-6 col-sm-4">';
//                            echo '<ul class="details-ticks">';
//                        }
//
//                        echo '<li><i class="jfont">&#xe815;</i>'.$row['Ozellik']['ozellik'].'</li>';
//
//                        $say++;
//
//                        if($say%4 == 0 || count($ilan['IlanOzellik']) == $say){
//                            echo '</ul>';
//                            echo '</div>';
//                        }
//                    }
//                    ?>
<!--                </div>-->

                <div class="row margin-top-45">
                    <div class="col-xs-12 apartment-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab-map" aria-controls="tab-map" role="tab" data-toggle="tab">
                                    <span>Harita</span>
                                    <div class="button-triangle2"></div>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#tab-street-view" aria-controls="tab-street-view" role="tab" data-toggle="tab">
                                    <span>Sokak Görünümü</span>
                                    <div class="button-triangle2"></div>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-map">
                                <div id="estate-map" class="details-map"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab-street-view">
                                <div id="estate-street-view" class="details-map"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(!empty($ilan['IlanDanisman'])){ ?>
                <div class="row margin-top-60">
                    <div class="col-xs-12">
                        <h3 class="title-negative-margin">İlan Danışmanları<span class="special-color">.</span></h3>
                        <div class="title-separator-primary"></div>
                    </div>
                </div>
                <div class="row margin-top-60">
                    <div class="row team-member-row">
                        <?php
                        foreach ($ilan['IlanDanisman'] as $row){
                        ?>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 team-member-col">
                                <div class="team-member">
                                    <div class="team-photo">
                                        <?php
                                        if(!empty($row['Danisman']['resim']) && file_exists($row['Danisman']['resim'])){
                                            echo '<img src="'.$this->webroot.$row['Danisman']['resim'].'" alt="" />';
                                        }else{
                                            echo '<img src="'.$this->webroot.'img/no-image.png" alt="" />';
                                        }
                                        ?>
                                        <div class="big-triangle"></div>
                                        <div class="big-triangle2"></div>
                                        <a class="big-icon big-icon-plus" href="agent-right-sidebar.html"><i class="jfont">&#xe804;</i></a>
                                        <div class="team-description">
                                            <div>
                                                <?php
                                                foreach ($row['Danisman']['DanismanIletisim'] as $cow){
                                                    echo '<div class="team-desc-line">';
                                                    echo '<span class="team-icon-circle">';
                                                    if($cow['type'] == 1){
                                                        echo '<i class="fa fa-phone"></i>';
                                                    }else if($cow['type'] == 3){
                                                        echo '<i class="fa fa-envelope fa-sm"></i>';
                                                    }
                                                    echo '</span>';
                                                    echo '<span>'.$cow['iletisim'].'</span>';
                                                    echo '</div>';
                                                }
                                                ?>

<!--                                                <p class="team-text">--><?php //echo CakeText::truncate($row['Danisman']['hakkinda'],100, array('html'=>true)); ?><!--</p>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="team-name">
                                        <h4><?=$row['Danisman']['adi'].' '.$row['Danisman']['soyadi']?><span class="special-color">.</span></h4>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <?php }?>
                <div class="margin-top-45"></div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
        mapInit(<?=$ilan['Ilan']['latitude']?>,<?=$ilan['Ilan']['longitude']?>,"estate-map","/images/pin-house.png", true);
        streetViewInit(<?=$ilan['Ilan']['latitude']?>,<?=$ilan['Ilan']['longitude']?>,"estate-street-view");

        // mapInit(41.2693,-70.0874,"grid-map1","images/pin-house.png", false);
        // mapInit(33.7544,-84.3857,"grid-map2","images/pin-apartment.png", false);
        // mapInit(33.7337,-84.4443,"grid-map3","images/pin-land.png", false);
        // mapInit(33.8588,-84.4858,"grid-map4","images/pin-commercial.png", false);
        // mapInit(34.0254,-84.3560,"grid-map5","images/pin-apartment.png", false);
        // mapInit(40.6128,-73.9976,"grid-map6","images/pin-house.png", false);
    }
</script>