<?php
$pagg = $this->passedArgs;
$this->Paginator->options(array('url' => $pagg));
?>
<section class="section-light section-top-shadow">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row">
                    <div class="col-xs-12 col-lg-6">
                        <h5 class="subtitle-margin">apartments for sale, colorodo, usa</h5>
                        <h1>42 estates found<span class="special-color">.</span></h1>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="view-icons-container">
                            <a class="view-box view-box-active"><img src="<?=$this->Html->url('/')?>emlak2/images/grid-icon.png" alt="" /></a>
                            <a class="view-box" href="listing-list-right-sidebar.html"><img src="<?=$this->Html->url('/')?>emlak2/images/list-icon.png" alt="" /></a>
                        </div>
                        <div class="order-by-container">
                            <select name="sort" class="bootstrap-select" title="Order By:">
                                <option>Price low to high</option>
                                <option>Price high to low</option>
                                <option>Area high to low</option>
                                <option>Area high to low</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="title-separator-primary"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="masonry-grid masonry-offers masonry-grid-short margin-top-60">
                            <!-- width of .grid-sizer used for columnWidth -->
                            <div class="masonry-grid-sizer"></div>
                            <?php
                            foreach ($ilanlar as $row){
                                ?>
                                <div class="masonry-grid-item">
                                    <a href="estate-details-right-sidebar.html" class="zoom-cont2">
                                        <div class="grid-offer-photo">
                                            <?php
                                            $image = $this->Html->url('/').'img/no-image.png';
                                            if(!empty($row['IlanResim']) && file_exists($row['IlanResim'][0]['paththumb'])){
                                                $image = $this->Html->url('/').$row['IlanResim'][0]['paththumb'];
                                            }
                                            ?>
                                            <img src="<?= $image?>" alt="" class="zoom" />
                                            <div class="type-container">
                                                <div class="estate-type">
                                                    <?php
                                                    if($row['Ilan']['ilan_tipi'] == 1){
                                                        echo 'Konut';
                                                    } else if($row['Ilan']['ilan_tipi'] == 2){
                                                        echo 'İşyeri';
                                                    } else if($row['Ilan']['ilan_tipi'] == 3){
                                                        echo 'Arsa';
                                                    }
                                                    ?>
                                                </div>
                                                <div class="transaction-type">
                                                    <?php
                                                    if($row['Ilan']['sat_kir'] == 1){
                                                        echo 'Satılık';
                                                    }else{
                                                        echo 'Kiralık';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-offer-text">
                                            <div class="grid-offer-h4"><h4 class="grid-offer-title"><?= $row['Ilan']['baslik'] ?></h4></div>
                                            <div class="clearfix"></div>
                                            <i class="fa fa-map-marker grid-offer-localization"></i>
                                            <div class="grid-offer-h4"><h4 class="grid-offer-title"><?= (!empty($row['Mahalle']['mahalle_adi'])?$row['Mahalle']['mahalle_adi'].', ':'').(!empty($row['Semt']['semt_adi'])?$row['Semt']['semt_adi'].', ':'').(!empty($row['Ilce']['ilce_adi'])?$row['Ilce']['ilce_adi'].', ':'').$row['Sehir']['sehir_adi'] ?></h4></div>
                                            <div class="clearfix"></div>
                                            <p><?=CakeText::truncate($row['Ilan']['aciklama'])?></p>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="price-grid-cont">
                                            <div class="grid-price-label pull-left">Fiyat:</div>
                                            <div class="grid-price pull-right">
                                                <i class="fa fa-try"></i> <?=number_format($row['Ilan']['fiyat'],0,'','.')?>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="grid-offer-params">
                                            <div class="grid-area">
                                                <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />54m<sup>2</sup>
                                            </div>
                                            <div class="grid-rooms">
                                                <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />3
                                            </div>
                                            <div class="grid-baths">
                                                <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />1
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
                <?php
                echo '<div class="offer-pagination margin-top-30">';
                echo $this->Paginator->prev( '<i class="jfont">&#xe800;</i>', array( 'class' => 'prev', 'tag' => false,'escape' => false), null, array( 'class' => 'disabled', 'tag' => null, 'disabledTag' => 'a','escape' => false) );
                echo $this->Paginator->numbers( array( 'tag' => false, 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a', 'escape' => false ) );
                echo $this->Paginator->next( '<i class="jfont">&#xe802;</i>', array( 'class' => 'next', 'tag' => false,'escape' => false ), null, array( 'class' => 'disabled', 'tag' => null, 'disabledTag' => 'a','escape' => false) );
                echo '<div class="clearfix"></div></div>';

                ?>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="sidebar">
                    <h3 class="sidebar-title">İlan Arama<span class="special-color">.</span></h3>
                    <div class="title-separator-primary"></div>

                    <div class="sidebar-select-cont">
                        <select name="sat_kir" class="bootstrap-select" title="Satılık-Kiralık:" multiple>
                            <option value="1">Satılık</option>
                            <option value="2">Kiralık</option>
                        </select>

                        <select name="ilan_tipi" class="bootstrap-select" title="İlan Tipi:" multiple>
                            <option value="1">Konut</option>
                            <option value="2">İşyeri</option>
                            <option value="3">Arsa</option>
                        </select>

                        <select name="sehir_id" class="bootstrap-select" title="Şehir:" multiple data-actions-box="true">
                            <?php
                            foreach ($sehir as $row){
                                echo '<option value="'.$row['Sehir']['id'].'">'.$row['Sehir']['sehir_adi'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-price-sidebar-value" class="adv-search-label">Fiyat:</label>
                        <span><i class="fa fa-try"></i></span>
                        <input type="text" name="fiyat" id="slider-range-price-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-price-sidebar" data-min="0" data-max="1000000" class="slider-range"></div>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-area-sidebar-value" class="adv-search-label">Alan:</label>
                        <span>m<sup>2</sup></span>
                        <input type="text" name="m_kare" id="slider-range-area-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-area-sidebar" data-min="0" data-max="10000" class="slider-range"></div>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-bedrooms-sidebar-value" class="adv-search-label">Oda:</label>
                        <input type="text" name="oda_sayisi" id="slider-range-bedrooms-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-bedrooms-sidebar" data-min="1" data-max="100" class="slider-range"></div>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-bathrooms-sidebar-value" class="adv-search-label">Banyo:</label>
                        <input type="text" name="banyo_sayisi" id="slider-range-bathrooms-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-bathrooms-sidebar" data-min="1" data-max="10" class="slider-range"></div>
                    </div>
                    <div class="sidebar-search-button-cont">
                        <a href="#" class="button-primary" id="searchButton">
                            <span>ara</span>
                            <div class="button-triangle"></div>
                            <div class="button-triangle2"></div>
                            <div class="button-icon"><i class="fa fa-search"></i></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function () {
    $('#searchButton').on('click',function(){
        window.location.href = '<?php echo $this->Html->url('/').'emlak2s/ilanlar_masonry'.$this->requestAction(array('controller'=>'emlak2s','action'=>'GetPagUrlWithoutSval'),$pagg);?>';
    });
});

</script>