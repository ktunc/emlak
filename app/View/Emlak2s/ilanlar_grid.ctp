<section class="section-light section-top-shadow">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row">
                    <div class="col-xs-12 col-lg-6">
                        <h5 class="subtitle-margin">apartments for sale, colorodo, usa</h5>
                        <h1>42 estates found<span class="special-color">.</span></h1>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="view-icons-container">
                            <a class="view-box view-box-active"><img src="<?=$this->Html->url('/')?>emlak2/images/grid-icon.png" alt="" /></a>
                            <a class="view-box" href="listing-list-right-sidebar.html"><img src="<?=$this->Html->url('/')?>emlak2/images/list-icon.png" alt="" /></a>
                        </div>
                        <div class="order-by-container">
                            <select name="sort" class="bootstrap-select" title="Order By:">
                                <option>Price low to high</option>
                                <option>Price high to low</option>
                                <option>Area high to low</option>
                                <option>Area high to low</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="title-separator-primary"></div>
                    </div>
                </div>
                <div class="row grid-offer-row">
                    <?php
                    foreach ($ilanlar as $row){
                    ?>
                        <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                            <div class="grid-offer">
                                <div class="grid-offer-front">

                                    <div class="grid-offer-photo">
                                        <img src="<?=$this->Html->url('/').$row['IlanResim'][0]['paththumb']?>" alt="" />
                                        <div class="type-container">
                                            <div class="estate-type">
                                                <?php
                                                if($row['Ilan']['ilan_tipi'] == 1){
                                                    echo 'Konut';
                                                } else if($row['Ilan']['ilan_tipi'] == 2){
                                                    echo 'İşyeri';
                                                } else if($row['Ilan']['ilan_tipi'] == 3){
                                                    echo 'Arsa';
                                                }
                                                ?>
                                            </div>
                                            <div class="transaction-type">
                                                <?php
                                                if($row['Ilan']['sat_kir'] == 1){
                                                    echo 'Satılık';
                                                }else{
                                                    echo 'Kiralık';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid-offer-text">
                                        <div class="grid-offer-h4"><h4 class="grid-offer-title"><?= $row['Ilan']['baslik'] ?></h4></div>
                                        <div class="clearfix"></div>
                                        <i class="fa fa-map-marker grid-offer-localization"></i>
                                        <div class="grid-offer-h5"><h4 class="grid-offer-title"><?= $row['Mahalle']['mahalle_adi'].', '.$row['Semt']['semt_adi'].', '.$row['Ilce']['ilce_adi'].', '.$row['Sehir']['sehir_adi'] ?></h4></div>
                                        <p><?=CakeText::truncate($row['Ilan']['aciklama'])?></p>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="price-grid-cont">
                                        <div class="grid-price-label pull-left">Fiyat:</div>
                                        <div class="grid-price pull-right">
                                            <?=number_format($row['Ilan']['fiyat'],0,'','.')?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grid-offer-params">
                                        <div class="grid-area">
                                            <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />54m<sup>2</sup>
                                        </div>
                                        <div class="grid-rooms">
                                            <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />3
                                        </div>
                                        <div class="grid-baths">
                                            <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />1
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-offer-back">
                                    <div id="grid-map1" class="grid-offer-map"></div>
                                    <div class="button">
                                        <a href="estate-details-right-sidebar.html" class="button-primary">
                                            <span>read more</span>
                                            <div class="button-triangle"></div>
                                            <div class="button-triangle2"></div>
                                            <div class="button-icon"><i class="fa fa-search"></i></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>

                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer2.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">apartment</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">West Fourth Street, New York 10003, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 299 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />48m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />2
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />1
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map2" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer3.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">apartment</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">E. Elwood St. Phoenix, AZ 85034, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 400 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />93m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />4
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />2
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map3" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer4.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">house</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">N. Willamette Blvd., Portland, OR 97203, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 800 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />300m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />8
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />3
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map4" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer5.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">apartment</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">One Brookings Drive St. Louis, Missouri 63130, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 320 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />50m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />2
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />1
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map5" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer7.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">house</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">One Neumann Drive Aston, Philadelphia 19014, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 500 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />210m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />6
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />2
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map6" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">

                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer1.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">apartment</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">34 Fort Collins, Colorado 80523, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 320 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />54m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />3
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />1
                                    </div>
                                </div>

                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map7" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer2.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">apartment</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">West Fourth Street, New York 10003, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 299 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />48m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />2
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />1
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map8" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer3.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">apartment</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">E. Elwood St. Phoenix, AZ 85034, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 400 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />93m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />4
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />2
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map9" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer4.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">house</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">N. Willamette Blvd., Portland, OR 97203, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 800 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />300m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />8
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />3
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map10" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer5.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">apartment</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">One Brookings Drive St. Louis, Missouri 63130, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 320 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />50m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />2
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />1
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map11" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
                        <div class="grid-offer">
                            <div class="grid-offer-front">
                                <div class="grid-offer-photo">
                                    <img src="<?=$this->Html->url('/')?>emlak2/images/grid-offer7.jpg" alt="" />
                                    <div class="type-container">
                                        <div class="estate-type">house</div>
                                        <div class="transaction-type">sale</div>
                                    </div>
                                </div>
                                <div class="grid-offer-text">
                                    <i class="fa fa-map-marker grid-offer-localization"></i>
                                    <div class="grid-offer-h4"><h4 class="grid-offer-title">One Neumann Drive Aston, Philadelphia 19014, USA</h4></div>
                                    <div class="clearfix"></div>
                                    <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="price-grid-cont">
                                    <div class="grid-price-label pull-left">Price:</div>
                                    <div class="grid-price pull-right">
                                        $ 500 000
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-offer-params">
                                    <div class="grid-area">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/area-icon.png" alt="" />210m<sup>2</sup>
                                    </div>
                                    <div class="grid-rooms">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/rooms-icon.png" alt="" />6
                                    </div>
                                    <div class="grid-baths">
                                        <img src="<?=$this->Html->url('/')?>emlak2/images/bathrooms-icon.png" alt="" />2
                                    </div>
                                </div>
                            </div>
                            <div class="grid-offer-back">
                                <div id="grid-map12" class="grid-offer-map"></div>
                                <div class="button">
                                    <a href="estate-details-right-sidebar.html" class="button-primary">
                                        <span>read more</span>
                                        <div class="button-triangle"></div>
                                        <div class="button-triangle2"></div>
                                        <div class="button-icon"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="offer-pagination margin-top-30">
                    <a href="#" class="prev"><i class="jfont">&#xe800;</i></a><a class="active">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#" class="next"><i class="jfont">&#xe802;</i></a>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="sidebar">
                    <h3 class="sidebar-title">narrow search<span class="special-color">.</span></h3>
                    <div class="title-separator-primary"></div>

                    <div class="sidebar-select-cont">
                        <select name="transaction1" class="bootstrap-select" title="Transaction:" multiple>
                            <option>For sale</option>
                            <option>For rent</option>
                        </select>
                        <select name="conuntry1" class="bootstrap-select" title="Country:" multiple data-actions-box="true">
                            <option>United States</option>
                            <option>Canada</option>
                            <option>Mexico</option>
                        </select>
                        <select name="city1" class="bootstrap-select" title="City:" multiple data-actions-box="true">
                            <option>New York</option>
                            <option>Los Angeles</option>
                            <option>Chicago</option>
                            <option>Houston</option>
                            <option>Philadelphia</option>
                            <option>Phoenix</option>
                            <option>Washington</option>
                            <option>Salt Lake Cty</option>
                            <option>Detroit</option>
                            <option>Boston</option>
                        </select>
                        <select name="location1" class="bootstrap-select" title="Location:" multiple data-actions-box="true">
                            <option>Some location 1</option>
                            <option>Some location 2</option>
                            <option>Some location 3</option>
                            <option>Some location 4</option>
                        </select>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-price-sidebar-value" class="adv-search-label">Price:</label>
                        <span>$</span>
                        <input type="text" id="slider-range-price-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-price-sidebar" data-min="0" data-max="300000" class="slider-range"></div>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-area-sidebar-value" class="adv-search-label">Area:</label>
                        <span>m<sup>2</sup></span>
                        <input type="text" id="slider-range-area-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-area-sidebar" data-min="0" data-max="180" class="slider-range"></div>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-bedrooms-sidebar-value" class="adv-search-label">Bedrooms:</label>
                        <input type="text" id="slider-range-bedrooms-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-bedrooms-sidebar" data-min="1" data-max="10" class="slider-range"></div>
                    </div>
                    <div class="adv-search-range-cont">
                        <label for="slider-range-bathrooms-sidebar-value" class="adv-search-label">Bathrooms:</label>
                        <input type="text" id="slider-range-bathrooms-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-bathrooms-sidebar" data-min="1" data-max="4" class="slider-range"></div>
                    </div>
                    <div class="sidebar-search-button-cont">
                        <a href="#" class="button-primary">
                            <span>search</span>
                            <div class="button-triangle"></div>
                            <div class="button-triangle2"></div>
                            <div class="button-icon"><i class="fa fa-search"></i></div>
                        </a>
                    </div>
                    <div class="sidebar-title-cont">
                        <h4 class="sidebar-title">featured offers<span class="special-color">.</span></h4>
                        <div class="title-separator-primary"></div>
                    </div>
                    <div class="sidebar-featured-cont">
                        <div class="sidebar-featured">
                            <a class="sidebar-featured-image" href="estate-details-right-sidebar.html">
                                <img src="<?=$this->Html->url('/')?>emlak2/images/sidebar-featured1.jpg" alt="" />
                                <div class="sidebar-featured-type">
                                    <div class="sidebar-featured-estate">A</div>
                                    <div class="sidebar-featured-transaction">S</div>
                                </div>
                            </a>
                            <div class="sidebar-featured-title"><a href="estate-details-right-sidebar.html">Fort Collins, Colorado 80523, USA</a></div>
                            <div class="sidebar-featured-price">$ 320 000</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="sidebar-featured">
                            <a class="sidebar-featured-image" href="estate-details-right-sidebar.html">
                                <img src="<?=$this->Html->url('/')?>emlak2/images/sidebar-featured2.jpg" alt="" />
                                <div class="sidebar-featured-type">
                                    <div class="sidebar-featured-estate">A</div>
                                    <div class="sidebar-featured-transaction">S</div>
                                </div>
                            </a>
                            <div class="sidebar-featured-title"><a href="estate-details-right-sidebar.html">West Fourth Street, New York 10003, USA</a></div>
                            <div class="sidebar-featured-price">$ 350 000</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="sidebar-featured">
                            <a class="sidebar-featured-image" href="estate-details-right-sidebar.html">
                                <img src="<?=$this->Html->url('/')?>emlak2/images/sidebar-featured3.jpg" alt="" />
                                <div class="sidebar-featured-type">
                                    <div class="sidebar-featured-estate">A</div>
                                    <div class="sidebar-featured-transaction">S</div>
                                </div>
                            </a>
                            <div class="sidebar-featured-title"><a href="estate-details-right-sidebar.html">E. Elwood St. Phoenix, AZ 85034, USA</a></div>
                            <div class="sidebar-featured-price">$ 410 000</div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="sidebar-title-cont">
                        <h4 class="sidebar-title">latest news<span class="special-color">.</span></h4>
                        <div class="title-separator-primary"></div>
                    </div>
                    <div class="sidebar-blog-cont">
                        <article>
                            <a href="blog-right-sidebar.html"><img src="<?=$this->Html->url('/')?>emlak2/images/footer-blog1.jpg" alt="" class="sidebar-blog-image" /></a>
                            <div class="sidebar-blog-title"><a href="blog-right-sidebar.html">This post title, lorem ipsum dolor sit</a></div>
                            <div class="sidebar-blog-date"><i class="fa fa-calendar-o"></i>28/09/15</div>
                            <div class="clearfix"></div>
                        </article>
                        <article>
                            <a href="blog-right-sidebar.html"><img src="<?=$this->Html->url('/')?>emlak2/images/footer-blog2.jpg" alt="" class="sidebar-blog-image" /></a>
                            <div class="sidebar-blog-title"><a href="blog-right-sidebar.html">This post title, lorem ipsum dolor sit</a></div>
                            <div class="sidebar-blog-date"><i class="fa fa-calendar-o"></i>28/09/15</div>
                            <div class="clearfix"></div>
                        </article>
                        <article>
                            <a href="blog-right-sidebar.html"><img src="<?=$this->Html->url('/')?>emlak2/images/footer-blog3.jpg" alt="" class="sidebar-blog-image" /></a>
                            <div class="sidebar-blog-title"><a href="blog-right-sidebar.html">This post title, lorem ipsum dolor sit</a></div>
                            <div class="sidebar-blog-date"><i class="fa fa-calendar-o"></i>28/09/15</div>
                            <div class="clearfix"></div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- google maps initialization -->
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
    //    var locations = [
    //        [40.6128,-73.9976, "<?//=$this->Html->url('/')?>//emlak2/images//pin-house.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer1.jpg", "Fort Collins, Colorado 80523, USA", "$320 000"],
    //        [40.7222,-73.7903, "<?//=$this->Html->url('/')?>//emlak2/images//pin-commercial.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer2.jpg", "West Fourth Street, New York 10003, USA", "$350 000"],
    //        [41.0306,-73.7669, "<?//=$this->Html->url('/')?>//emlak2/images//pin-house.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer3.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$300 000"],
    //        [41.3006,-72.9440, "<?//=$this->Html->url('/')?>//emlak2/images//pin-apartment.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer4.jpg", "Fort Collins, Colorado 80523, USA", "$410 000"],
    //        [42.2418,-74.3626, "<?//=$this->Html->url('/')?>//emlak2/images//pin-land.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer5.jpg", "West Fourth Street, New York 10003, USA", "$295 000"],
    //        [38.8974,-77.0365, "<?//=$this->Html->url('/')?>//emlak2/images//pin-house.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer6.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$390 600"],
    //        [38.7860,-77.0129, "<?//=$this->Html->url('/')?>//emlak2/images//pin-apartment.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer7.jpg", "Fort Collins, Colorado 80523, USA", "$299 000"],
    //        [41.2693,-70.0874, "<?//=$this->Html->url('/')?>//emlak2/images//pin-house.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer8.jpg", "Fort Collins, Colorado 80523, USA", "$600 000"],
    //        [33.7544,-84.3857, "<?//=$this->Html->url('/')?>//emlak2/images//pin-commercial.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer9.jpg", "West Fourth Street, New York 10003, USA", "$350 000"],
    //        [33.7337,-84.4443, "<?//=$this->Html->url('/')?>//emlak2/images//pin-house.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer10.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$550 000"],
    //        [33.8588,-84.4858, "<?//=$this->Html->url('/')?>//emlak2/images//pin-land.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer1.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$670 000"],
    //        [34.0254,-84.3560, "<?//=$this->Html->url('/')?>//emlak2/images//pin-commercial.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer5.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$300 000"],
    //        [39.6282,-86.1320, "<?//=$this->Html->url('/')?>//emlak2/images//pin-apartment.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer6.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$390 600"],
    //        [40.5521,-84.5658, "<?//=$this->Html->url('/')?>//emlak2/images//pin-apartment.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer2.jpg", "West Fourth Street, New York 10003, USA", "$350 000"],
    //        [41.6926,-87.6021, "<?//=$this->Html->url('/')?>//emlak2/images//pin-house.png", "estate-details-right-sidebar.html", "<?//=$this->Html->url('/')?>//emlak2/images//infobox-offer5.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$300 000"]
    //    ];
    //    offersMapInit("offers-map",locations);

        mapInit(41.2693,-70.0874,"grid-map1","<?=$this->Html->url('/')?>emlak2/images//pin-house.png", false);
        mapInit(33.7544,-84.3857,"grid-map2","<?=$this->Html->url('/')?>emlak2/images//pin-apartment.png", false);
        mapInit(33.7337,-84.4443,"grid-map3","<?=$this->Html->url('/')?>emlak2/images//pin-land.png", false);
        mapInit(33.8588,-84.4858,"grid-map4","<?=$this->Html->url('/')?>emlak2/images//pin-commercial.png", false);
        mapInit(34.0254,-84.3560,"grid-map5","<?=$this->Html->url('/')?>emlak2/images//pin-apartment.png", false);
        mapInit(40.6128,-73.9976,"grid-map6","<?=$this->Html->url('/')?>emlak2/images//pin-house.png", false);
        mapInit(40.6128,-73.7903,"grid-map7","<?=$this->Html->url('/')?>emlak2/images//pin-house.png", false);
        mapInit(40.7222,-73.7903,"grid-map8","<?=$this->Html->url('/')?>emlak2/images//pin-apartment.png", false);
        mapInit(41.0306,-73.7669,"grid-map9","<?=$this->Html->url('/')?>emlak2/images//pin-land.png", false);
        mapInit(41.3006,-72.9440,"grid-map10","<?=$this->Html->url('/')?>emlak2/images//pin-commercial.png", false);
        mapInit(42.2418,-74.3626,"grid-map11","<?=$this->Html->url('/')?>emlak2/images//pin-house.png", false);
        mapInit(38.8974,-77.0365,"grid-map12","<?=$this->Html->url('/')?>emlak2/images//pin-apartment.png", false);
    }
</script>