<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?=$this->Session->read('UserName')?></strong>
                             </span> <span class="text-muted text-xs block"><?=$this->Session->read('User_Name')?> <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="<?=$this->Html->url('/')?>logout">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Konut</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/ilanlar?ilan_tipi=1">Konut İlanları</a></li>
                    <li><a href="<?=$this->Html->url('/')?>admins/ilanyeni?tip=1">Yeni Konut</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">İş Yeri</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/ilanlar?ilan_tipi=2">İş Yeri İlanları</a></li>
                    <li><a href="<?=$this->Html->url('/')?>admins/ilanyeni?tip=2">Yeni İş Yeri</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Arsa</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/ilanlar?ilan_tipi=3">Arsa İlanları</a></li>
                    <li><a href="<?=$this->Html->url('/')?>admins/ilanyeni?tip=3">Yeni Arsa</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Haber</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/haberler">Haberler</a></li>
                    <li><a href="<?=$this->Html->url('/')?>admins/haberyeni">Yeni Haber</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Projeler</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/projeler">Projeler</a></li>
                    <li><a href="<?=$this->Html->url('/')?>admins/projeyeni">Yeni Proje</a></li>
                </ul>
            </li>
            <?php if($this->Session->read('UserGroup') == 1){ ?>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Danışmanlar</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/danismanlar">Danışmanlar</a></li>
                    <li><a href="<?=$this->Html->url('/')?>admins/danismanyeni">Yeni Danışman</a></li>
                </ul>
            </li>
            <?php } ?>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">İlan Özellikleri</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/ozellikler">İlan Özellikleri</a></li>
                </ul>
            </li>
            <?php if($this->Session->read('UserGroup') == 1){ ?>
            <li>
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Kullanıcılar</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?=$this->Html->url('/')?>admins/userlar">Kullanıcılar</a></li>
                    <li><a href="<?=$this->Html->url('/')?>admins/useryeni">Yeni Kullanıcı</a></li>
                </ul>
            </li>
            <?php } ?>
            <li>
                <a href="<?=$this->Html->url('/')?>admins/logout"><i class="fa fa-sign-out"></i> <span class="nav-label">Çıkış</span></a>
            </li>
        </ul>
    </div>
</nav>