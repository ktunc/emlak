<?php
if(!empty($data)){
?>
<div class="details-parameters">

    <?php
    if(!empty($data['m_kare'])){
    ?>
        <div class="details-parameters-cont">
            <div class="details-parameters-name">Alan</div>
            <div class="details-parameters-val"><?=$data['m_kare']?>m<sup>2</sup></div>
            <div class="clearfix"></div>
        </div>
    <?php
    }
    ?>

    <?php
    if(!empty($data['oda_sayisi'])){
        ?>
        <div class="details-parameters-cont">
            <div class="details-parameters-name">Oda</div>
            <div class="details-parameters-val"><?=$data['oda_sayisi']?></div>
            <div class="clearfix"></div>
        </div>
        <?php
    }
    ?>

    <?php
    if(!empty($data['banyo_sayisi'])){
        ?>
        <div class="details-parameters-cont">
            <div class="details-parameters-name">Banyo</div>
            <div class="details-parameters-val"><?=$data['banyo_sayisi']?></div>
            <div class="clearfix"></div>
        </div>
        <?php
    }
    ?>

    <?php
    if(!empty($data['kat'])){
        ?>
        <div class="details-parameters-cont">
            <div class="details-parameters-name">Bulunduğu Kat</div>
            <div class="details-parameters-val"><?=$data['kat']?></div>
            <div class="clearfix"></div>
        </div>
        <?php
    }
    ?>

    <?php
    if(!empty($data['bina_kat'])){
        ?>
        <div class="details-parameters-cont">
            <div class="details-parameters-name">Bina Kat</div>
            <div class="details-parameters-val"><?=$data['bina_kat']?></div>
            <div class="clearfix"></div>
        </div>
        <?php
    }
    ?>

</div>
<?php
}
?>
