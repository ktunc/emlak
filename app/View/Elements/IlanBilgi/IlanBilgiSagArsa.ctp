<div class="details-parameters">
    <div class="details-parameters-cont">
        <div class="details-parameters-name">Alan</div>
        <div class="details-parameters-val">54m<sup>2</sup></div>
        <div class="clearfix"></div>
    </div>
    <div class="details-parameters-cont">
        <div class="details-parameters-name">bedrooms</div>
        <div class="details-parameters-val">3</div>
        <div class="clearfix"></div>
    </div>
    <div class="details-parameters-cont">
        <div class="details-parameters-name">bathrooms</div>
        <div class="details-parameters-val">1</div>
        <div class="clearfix"></div>
    </div>
    <div class="details-parameters-cont">
        <div class="details-parameters-name">parking places</div>
        <div class="details-parameters-val">2</div>
        <div class="clearfix"></div>
    </div>
    <div class="details-parameters-cont">
        <div class="details-parameters-name">Lorem</div>
        <div class="details-parameters-val">nostrud</div>
        <div class="clearfix"></div>
    </div>
    <div class="details-parameters-cont">
        <div class="details-parameters-name">Ipsum</div>
        <div class="details-parameters-val">tempor</div>
        <div class="clearfix"></div>
    </div>
    <div class="details-parameters-cont details-parameters-cont-last">
        <div class="details-parameters-name">Consectetur</div>
        <div class="details-parameters-val">eiusmod</div>
        <div class="clearfix"></div>
    </div>
</div>