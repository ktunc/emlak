<div class="form-group">
    <label for="oda" class="col-xs-3 col-md-2 control-label">Oda Sayısı</label>
    <div class="col-xs-9 col-md-10">
        <input type="text" name="oda" id="oda" placeholder="Oda Sayısı" class="form-control" value="<?=!empty($data)?$data['oda_sayisi']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label for="oda" class="col-xs-3 col-md-2 control-label">Banyo Sayısı</label>
    <div class="col-xs-9 col-md-10">
        <input type="text" name="banyo" id="banyo" placeholder="Banyo Sayısı" class="form-control" value="<?=!empty($data)?$data['banyo_sayisi']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label for="m2" class="col-xs-3 col-md-2 control-label">Metre Kare</label>
    <div class="col-xs-9 col-md-10">
        <input type="number" name="m2" id="m2" placeholder="Metre Kare" class="form-control" value="<?=!empty($data)?$data['m_kare']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label for="kat" class="col-xs-3 col-md-2 control-label">Kaçıncı Katta</label>
    <div class="col-xs-9 col-md-10">
        <input type="text" name="kat" id="kat" placeholder="Kaçıncı Katta" class="form-control" value="<?=!empty($data)?$data['kat']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label for="binakat" class="col-xs-3 col-md-2 control-label">Bina Katı</label>
    <div class="col-xs-9 col-md-10">
        <input type="number" name="binakat" id="binakat" placeholder="Bina Katı" class="form-control" value="<?=!empty($data)?$data['bina_kat']:''?>"/>
    </div>
</div>