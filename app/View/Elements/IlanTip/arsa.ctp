<div class="form-group">
    <label class="col-xs-3 col-md-2 control-label" for="imar">İmar Durumu</label>
    <div class="col-xs-9 col-md-10">
        <input type="text" name="imar" id="imar" placeholder="İmar Durumu" class="form-control" value="<?=!empty($data)?$data['imar_durum']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label class="col-xs-3 col-md-2 control-label" for="m2">Metre Kare</label>
    <div class="col-xs-9 col-md-10">
        <input type="number" name="m2" id="m2" placeholder="Metre Kare" class="form-control" value="<?=!empty($data)?$data['m_kare']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label class="col-xs-3 col-md-2 control-label" for="ada">Ada</label>
    <div class="col-xs-9 col-md-10">
        <input type="text" name="ada" id="ada" placeholder="Ada" class="form-control" value="<?=!empty($data)?$data['ada']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label class="col-xs-3 col-md-2 control-label" for="parsel">Parsel</label>
    <div class="col-xs-9 col-md-10">
        <input type="text" name="parsel" id="parsel" placeholder="Parsel" class="form-control" value="<?=!empty($data)?$data['parsel']:''?>"/>
    </div>
</div>
<div class="form-group">
    <label class="col-xs-3 col-md-2 control-label" for="tapu">Tapu</label>
    <div class="col-xs-9 col-md-10">
        <input type="text" name="tapu" id="tapu" placeholder="Tapu" class="form-control" value="<?=!empty($data)?$data['tapu']:''?>"/>
    </div>
</div>