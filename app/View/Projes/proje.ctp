<?php 
echo $this->Html->css('photoswipe');
echo $this->Html->script('simple-inheritance.min');
echo $this->Html->script('code-photoswipe-1.0.11');
echo $this->Html->css('../js/Flickerplate-master/css/flickerplate');
echo $this->Html->script('Flickerplate-master/js/min/modernizr-custom-v2.7.1.min');
echo $this->Html->script('Flickerplate-master/js/min/jquery-finger-v0.1.0.min');
echo $this->Html->script('Flickerplate-master/js/min/flickerplate.min');

echo $this->Html->script('locationpicker.jquery.notdrag');
echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&libraries=places');
?>

<div class="bg-white printwidth">
<div class="ilanUst row">
    <div class="col-xs-12 text-success h3 fontBold"><?php echo $projes['Proje']['baslik'];?></div>
    <div class="h5 col-xs-12"><?php echo date('d.m.Y H:i:s',strtotime($projes['Proje']['tarih']));?></div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="flicker-example flickCSS" data-block-text="false">
            <ul id="Gallery">
                <?php
                if(count($projes['ProjeResim'])>0){
                    foreach($projes['ProjeResim'] as $row){
                         echo '<li style="background-size:100% 100%">'
                                 .'<div class="flick-title">'
                                 . '<a href="'.$this->Html->url('/').$row['path'].'">'
                                 . '<img class="imgIcerik" src="'.$this->Html->url('/').$row['path'].'" />'
                                 . '</a>'
                                 . '</div>'
                                 . '</li>';
                    }
                }else{
                    echo '<li style="background-size:100% 100%">'
                        .'<div class="flick-title">'
                        . '<a href="'.$this->Html->url('/').'img/logo.png">'
                        . '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png"/>'
                        . '</a>'
                        . '</div>'
                        . '</li>';
                } ?>
            </ul>
            <div class="IlanShareIcon">
                <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
            </div>
            <?php
            if(!empty($projes['Proje']['video'])){
                ?>
                <div class="IlanVideoIcon">
                    <button id="videoGetir" class="btn btn-primary btn-sm"><i class="fa fa-video-camera fa-2x"></i></button>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<input type="hidden" id="projeId" value="<?php echo $projes['Proje']['id']; ?>" />

<div class="ilanIcerik col-xs-12" style="text-align: left;">
    <?php echo nl2br($projes['Proje']['aciklama']);?>
</div>

    <div class="row">
        <div class="col-xs-12">
            <div class="gmap" id="us2"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "<?php echo $projes['Proje']['baslik'];?>";
    
Code.photoSwipe('a', '#Gallery');

$('.flicker-example').flicker({
    auto_flick: true,
    auto_flick_delay: 5,
    flick_animation: "transform-slide"
});

    $('#us2').locationpicker({
	location: {latitude: <?php echo empty($projes['Proje']['latitude'])?0:$projes['Proje']['latitude'];?>, longitude: <?php echo empty($projes['Proje']['longitude'])?0:$projes['Proje']['longitude'];?>},
	radius: 10,
        zoom:17,
        draggable:false
    });

    $('#ShareBut').on('click',function(){
        $('#ShareIlanModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
});
</script>