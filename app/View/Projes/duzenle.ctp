<?php
echo $this->Html->script('locationpicker.jquery');
echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&libraries=places');

?>
<div class="container">
<form method="POST" role="form" class="form-signin" action="<?php echo $this->Html->url('/');?>projes/upgradeproje" id="ProjeForm" enctype="multipart/form-data">
    <input type="hidden" name="projeId" value="<?php echo $projes['Proje']['id'] ?>"/>
    <label for="baslik">Başlık</label>
    <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control" value="<?php echo $projes['Proje']['baslik'] ?>" />
    <label for="aciklama">Açıklama</label>
    <textarea name="aciklama" id="aciklama" placeholder="Açıklama" class="form-control" rows="10"><?php echo $projes['Proje']['aciklama']; ?></textarea>
    <br>
    <!-- Video -->
    <label for="video">Video</label>
    <table id="videolar"></table>
    <?php 
    if(!empty($projes['Proje']['video'])){
    ?>
        <video width="100%" height="240" controls id="videoGoster">
            <source src="<?php echo $this->Html->url('/').$projes['Proje']['video'];?>" type="video/mp4">
        </video><br>
        <button class="btn btn3d btn-danger" type="button" id="deleteVideo" onclick="delVideo(<?php echo $projes['Proje']['id'];?>)">Video Sil</button>
        <button class="btn btn3d btn-warning" style="display:none" type="button" id="addVideo">Video Ekle</button>
    <?php
    }else{
        echo '<button class="btn btn3d btn-warning" type="button" id="addVideo">Video Ekle</button>';
    }
    ?>
    <br>
    <!-- Resim -->
    <label for="resim">Resimler</label>
    <div class="row">
        <?php 
        foreach($projeRes as $cow){
        ?>
        <div class="col-sm-6 col-md-3 resims" id="Res_<?php echo $cow['ProjeResim']['id'];?>">
            <div class="thumbnail" style="background-color: #FFFFFF" >
                <img src="<?php echo $this->Html->url('/').$cow['ProjeResim']['path'];?>" alt="" style="width:300px;height:200px;" />
                <button type="button" class="btn btn3d btn-danger" onclick="DeleteResim('<?php echo $cow['ProjeResim']['id'];?>')">Sil</button>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
    <table id="resler">
        
    </table>
    <button class="btn btn3d btn-warning" type="button" id="addResim">Resim Ekle</button>
    <br>
    <br>
    <label for="us2-address">Location</label>
    <input type="text" id="us2-address" name="location"  class="form-control"/>
    <div class="col-xs-12" id="us2" style="min-height: 300px;margin-top: 2%;"></div>
    <input type="hidden" id="us2-lat" name="latitude" />
    <input type="hidden" id="us2-lon" name="longitude" />

    <div class="col-xs-12" style="margin-top:5%">
        <button type="button" class="btn btn3d btn-primary" id="ProjeKaydet">Kaydet</button>
        <button type="button" class="btn btn3d btn-default" id="ProjeIptal">İptal</button>
        <button type="button" class="btn btn3d btn-danger" style="float:right" onclick="ProjeSil(<?php echo $projes['Proje']['id'];?>)">Sil</button>
    </div>
</form>
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
var resimTR = '<tr><td><input type="file" name="projeRes[]" class="form-control"/></td>\n\
<td><button type="button" class="btn btn3d btn-danger" id="resIptal">İptal</button></td></tr>';
$(document).ready(function(){
    tinymce.init({
        selector: '#aciklama',
        language: 'tr_TR',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });

    $('#addResim').on('click',function(){
        if(($('input[name="projeRes[]"]').length+$('.resims').length) >= 20){
            alert('20 taneden fazla resim eklenemez.');
        }else{
            $('#resler').append(resimTR);
        }
    });
    
    $('#resler').on('click','#resIptal',function(){
       $(this).closest('tr').remove();
    });
    
    $('#addVideo').on('click',function(){
        $('#videolar').append('<tr><td><input type="file" name="projeVideo" class="form-control"/></td>\n\
        <td><button type="button" class="btn btn3d btn-danger btn-small" id="videoIptal">İptal</button></td></tr>');
        $('#addVideo').hide();
    });
    
    $('#videolar').on('click','#videoIptal',function(){
        $(this).closest('tr').remove();
        $('#addVideo').show();
    });
    
    $('#ProjeKaydet').on('click',function(){
        if($('#baslik').val() == ''){
            alert('Başlık boş bırakılamaz.');
        }
        else{
            $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
            $('#ProjeForm').submit();
        }
    });
    
    $('#us2').locationpicker({
	location: {latitude: <?php echo empty($projes['ProjeLocation']['latitude'])?0:$projes['ProjeLocation']['latitude'];?>, longitude: <?php echo empty($projes['ProjeLocation']['longitude'])?0:$projes['ProjeLocation']['longitude'];?>},
	radius: 10,
	inputBinding: {
        latitudeInput: $('#us2-lat'),
        longitudeInput: $('#us2-lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2-address')
        },
        enableAutocomplete: true
    });
    
    $('#ProjeIptal').on('click',function(){
       window.location.href = '<?php echo $this->Html->url('/');?>projes/home';
    });
});

function DeleteResim(resId){
    if(confirm('Resmi silmek istediğinizden emin misiniz?')){
        $.ajax({
           async: false,
           type: 'POST',
           url: "<?php echo $this->Html->url('/');?>projes/deleteResim",
           data: 'resId='+resId,
           success: function (data) {
               var dat = $.parseJSON(data);
               if(dat){
                   $('#Res_'+resId).remove();
               }else{
                   alert('Bir hata meydana geldi. Lütfen tekrar deneyin.');
               }
            }
        });
    }
}

function ProjeSil(projeId){
    if(confirm('Projeyi silmek istediğinizden emin misiniz?')){
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>projes/ajaxDeleteProje",
            data:"projeId="+projeId,
            success: function (data) {
                $.unblockUI();
                        alert('Proje silme işlemi başarıyla gerçekleşti.');
                        window.location.href="<?php echo $this->Html->url('/')?>projes/home";
                    }
        });
    }else{
        return false;
    }
}

function delVideo(projeId){
    if(confirm('Videoyu silmek istediğinizden emin misiniz?')){
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>projes/ajaxDeleteVideo",
            data:"projeId="+projeId,
            success: function (data) {
                var dat = $.parseJSON(data);
                if(dat){
                    $('#videoGoster').remove();
                    $('#deleteVideo').remove();
                    $('#addVideo').show();
                }else{
                    alert('Bir hata meydana geldi. Lütfen tekrar deneyin.');
                }
            }
        });
    }else{
        return false;
    }
}
</script>