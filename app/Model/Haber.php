<?php
class Haber extends AppModel{
    var $useTable = 'em_haber';
    public $actsAs = array('Containable');

    public $hasMany = array(
        'HaberResim' => array(
            'className' => 'HaberResim',
            'foreignKey' => 'haber_id'
        )
    );
}