<?php
class DanismanIletisim extends AppModel{
    var $useTable = "em_danisman_iletisim";

    public $belongsTo = array(
        'IlanIletisimType' => array(
            'className' => 'IlanIletisimType',
//            'foreignKey' => false,
//            'conditions' => array('DanismanIletisim.type = IlanIletisimType.id'),
            'foreignKey' => 'type',
            'bindingKey'=>'id',
            'dependent' => true
        )
    );
}