<?php
class IlanDanisman extends AppModel{
    var $useTable = "em_ilan_danisman";
    public $actsAs = array('Containable');

    public $belongsTo = array(
        'Danisman' => array(
            'className'=>'Danisman',
            'foreignKey' => 'danisman_id',
            'bindingKey' => 'id',
            'dependent' => true
        )
    );
}