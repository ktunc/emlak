<?php
class IlanOzellik extends AppModel{
    var $useTable = "em_ilan_ozellik";
    public $actsAs = array('Containable');

    public $belongsTo = array(
        'Ozellik' => array(
            'className'=>'Ozellik',
            'foreignKey' => 'ozellik_id',
            'bindingKey' => 'id',
            'dependent' => true
        )
    );
}