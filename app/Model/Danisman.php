<?php
class Danisman extends AppModel{
    public $useTable = "em_danisman";
    public $actsAs = array('Containable');

    public $hasMany = array(
        'DanismanIletisim' => array(
            'className' => 'DanismanIletisim',
            'foreignKey' => 'danisman_id',
            'order' => 'DanismanIletisim.type ASC'
        )
    );
}