<?php
/**
 * @property Sehir $Sehir
 * @property Ilce $Ilce
 * @property Mahalle $Mahalle
 * @property Semt $Semt
 * @property IlanNo $IlanNo
 * @property User $User
 * @property Haber $Haber
 * @property HaberResim $HaberResim
 * @property Proje $Proje
 * @property ProjeResim $ProjeResim
 * @property Danisman $Danisman
 * @property DanismanIletisim $DanismanIletisim
 * @property IlanIletisimType $IlanIletisimType
 * @property Ilan $Ilan
 * @property KonutBilgi $KonutBilgi
 * @property ArsaBilgi $ArsaBilgi
 * @property IsyeriBilgi $IsyeriBilgi
 * @property IlanDanisman $IlanDanisman
 * @property Ozellik $Ozellik
 * @property IlanOzellik $IlanOzellik
 */
App::uses('Security', 'Utility');
App::uses ( 'String', 'Utility' );
App::uses('ImageManipulator','Vendor');
App::uses('FilerUploader','Vendor');
class AdminsController extends AppController{
    var $uses = array('Ilan','IlanResim', 'KonutBilgi', 'ArsaBilgi', 'IsyeriBilgi', 'IlanDanisman', 'Tip','Sehir','Ilce','Mahalle','Semt',
        'IlanIletisim','IlanIletisimType', 'User', 'Haber', 'HaberResim', 'Proje', 'ProjeResim', 'ProjeLocation', 'Danisman', 'DanismanIletisim','Ozellik', 'IlanOzellik');
    public $components = array('Paginator','HtmlMeta');
    public $IlanTipArr = array(1=>'Konut',2=>'İşyeri',3=>'Arsa');
    var $layout = 'admin';
    public function giris(){
        if($this->Session->check('UserLogin')){
            $this->redirect(array('controller'=>'admins','action'=>'index'));
        }
        $this->layout = 'giris';
    }

    public function login(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $username = $this->request->data('username');
            $pass = $this->request->data('pass');
            $pass = Security::hash($pass, 'sha1', true);

            $data = $this->User->findByUsernameAndPassAndDurum($username,$pass,1);
            if($data){
                $this->Session->write('UserLogin', true);
                $this->Session->write('UserId', $data['User']['id']);
                $this->Session->write('UserGroup', $data['User']['group']);
                $this->Session->write('UserName', $data['User']['name']);
                $this->Session->write('User_Name', $data['User']['username']);
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Giriş başarılı.','default', array('class'=>'alert alert-success alert-dismissable'));
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'ilanlar')
                );
            }
            else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Giriş başarısız.','default', array('class'=>'alert alert-danger alert-dismissable'));
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'giris')
                );
            }
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Giriş başarısız.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'giris')
            );
        }
    }

    public function logout(){
        $this->autoRender = false;
        $this->Session->destroy('all');
        return $this->redirect(
            array('controller' => 'users', 'action' => 'index')
        );
    }

    public function index(){

    }

    // Filer image Bas
    public function uploadimage(){
        $this->autoRender = false;
        $uploader = new FilerUploader();
        $data = $uploader->upload($_FILES['files'], array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => 'img/gecici/', //Upload directory {String}
            'title' => array('randomVEtime'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => false, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));

        if($data['isComplete']){
            $files = $data['data'];

            echo json_encode($files['metas'][0]['name']);
        }

        if($data['hasErrors']){
            $errors = $data['errors'];
            echo json_encode($errors);
        }
    }

    public function removeimage(){
        $this->autoRender = false;
        if(isset($_POST['file'])){
            $file = 'img/gecici/' . $_POST['file'];
            if(file_exists($file)){
                unlink($file);
            }
        }
        echo json_encode(true);
    }
    // Filer image SON

    // Haberler Bas
    function haberler(){
        $haberler = $this->Haber->find('all');
        $this->set('haberler',$haberler);
    }

    function haberedit(){
        $get = $this->request->query;
        if(array_key_exists('haber_id',$get)){

            $habers = $this->Haber->find('first',
                array(
                    'fields'=>array('*'),
                    'contain'=>array('HaberResim'),
                    'conditions'=>array("Haber.id"=>$get['haber_id'])

                ));

            if(!$habers){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Aramış olduğunuz ilan yok veya kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'haberler')
                );
            }
            $this->set('habers',$habers);

            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Aramış olduğunuz ilan yok veya kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'haberler')
            );
        }
    }

    function haberdeleteresim(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->HaberResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['HaberResim']['path']);
            $fileRes->delete();
            if($this->HaberResim->deleteAll(array('id'=>$ResId))){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
        exit();
    }

    function habersil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $haberId = $this->request->data('haberId');
            $haberRes = $this->HaberResim->findAllByHaberId($haberId);
            foreach($haberRes as $row){
                $fileRes = new File($row['HaberResim']['path']);
                $fileRes->delete();
                $fileResThumb = new File($row['HaberResim']['paththumb']);
                $fileResThumb->delete();
            }

            $this->HaberResim->deleteAll(array('haber_id'=>$haberId));
            $this->Haber->deleteAll(array('id'=>$haberId));
            $return['hata'] = false;
        }
        echo json_encode($return);
        exit();
    }

    function haberyeni(){}

    function haberkaydet(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')) {
            $data = $this->request->data;

            if(!array_key_exists('haber_id', $data)){
                $return['mesaj'] = 'Haber eklerken bir hata meydana geldi. Lütfen tekrar deneyin.';
                $return['link'] = Router::url('/',true).'admins';
                echo json_encode($return);
                exit();
            }

            $haber_id = 0;
            if($data['haber_id'] != 0){
                $this->Haber->id = $data['haber_id'];
                $haber_id = $data['haber_id'];
            }else{
                $this->Haber->create();
            }

            $yuklenenfile = array_key_exists('yuklenenfile', $data) ? $data['yuklenenfile'] : false;
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $saveData = array(
                'baslik' => $baslik,
                'aciklama' => $aciklama
            );

            if($this->Haber->save($saveData)){
                if($haber_id == 0){
                    $haber_id = $this->Haber->getInsertID();
                }

                $hataRes = 0;
                if($yuklenenfile){
                    foreach($yuklenenfile as $value){
                        $asilpath = 'img/gecici/'.$value;
                        if(is_file($asilpath)){
                            $fileType = pathinfo($value, PATHINFO_EXTENSION);
                            $asilname = date('YmdHis').uniqid();
                            $name = $asilname.'.'.$fileType;
                            $nameThumb = $asilname.'_thumb.'.$fileType;
                            $this->HaberResim->create();
                            $this->HaberResim->save(array('haber_id'=>$haber_id));
                            $lastId = $this->HaberResim->getLastInsertID();
                            $path = 'img/haber/'.$haber_id.'/'.$name;
                            $pathThumb = 'img/haber/'.$haber_id.'/'.$nameThumb;
                            //image Resize
                            $manipulator = new ImageManipulator($asilpath);
                            $newImage = $manipulator->resample(800, 600, false);
                            if($manipulator->save($path)){
                                $this->HaberResim->id = $lastId;
                                $this->HaberResim->save(array('path'=>$path));
                                $manipulator = new ImageManipulator($path);
                                $newImage = $manipulator->resample(200, 200);
                                if($manipulator->save($pathThumb)){
                                    $this->HaberResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                    if(file_exists($asilpath)){
                                        unlink($asilpath);
                                    }
                                }
                            }else{
                                $hataRes++;
                                $this->HaberResim->deleteAll(array('id'=>$lastId));
                            }
                        }else{
                            $hataRes++;
                        }
                    }
                }

                if($hataRes>0){
                    $return['mesaj'] = 'Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.';
                    $return['link'] = Router::url('/',true).'admins/haberedit?haber_id='.$haber_id;
                    echo json_encode($return);
                    exit();
                }else{
                    $return['hata'] = false;
                    $return['mesaj'] = 'Haber başarıyla kaydedildi.';
                    $return['link'] = Router::url('/',true).'admins/haberedit?haber_id='.$haber_id;
                    echo json_encode($return);
                    exit();
                }
            }else{
                $return['mesaj'] = 'Bir hata meydana geldi. Lütfen tekrar deneyin.';
                $return['link'] = false;
                echo json_encode($return);
                exit();
            }
        }else{
            $return['mesaj'] = 'Hata!!!';
            $return['link'] = Router::url('/',true);
            echo json_encode($return);
            exit();
        }
    }

    // Haberler Son

    // Projeler Bas
    function projeler(){
        $projeler = $this->Proje->find('all');
        $this->set('projeler',$projeler);
    }

    function projeedit(){
        $get = $this->request->query;
        if(array_key_exists('proje_id',$get)){

            $projes = $this->Proje->find('first',
                array(
                    'fields'=>array('*'),
                    'contain'=>array('ProjeResim'),
                    'conditions'=>array("Proje.id"=>$get['proje_id'])
                ));

            if(!$projes){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Aramış olduğunuz ilan yok veya kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'projeler')
                );
            }
            $this->set('projes',$projes);
            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Aramış olduğunuz ilan yok veya kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'projeler')
            );
        }
    }

    function projedeleteresim(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->ProjeResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['ProjeResim']['path']);
            $fileRes->delete();
            if($this->ProjeResim->deleteAll(array('id'=>$ResId))){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
        exit();
    }

    function projesil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $projeId = $this->request->data('projeId');
            $projeRes = $this->ProjeResim->findAllByProjeId($projeId);
            foreach($projeRes as $row){
                $fileRes = new File($row['ProjeResim']['path']);
                $fileRes->delete();
                $fileResThumb = new File($row['ProjeResim']['paththumb']);
                $fileResThumb->delete();
            }

            $this->ProjeResim->deleteAll(array('proje_id'=>$projeId));
            $this->Proje->deleteAll(array('id'=>$projeId));
            $return['hata'] = false;
        }
        echo json_encode($return);
        exit();
    }

    function projeyeni(){}

    function projekaydet(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')) {
            $data = $this->request->data;

            if(!array_key_exists('proje_id', $data)){
                $return['mesaj'] = 'Proje eklerken bir hata meydana geldi. Lütfen tekrar deneyin.';
                $return['link'] = Router::url('/',true).'admins';
                echo json_encode($return);
                exit();
            }

            $proje_id = 0;
            if($data['proje_id'] != 0){
                $this->Proje->id = $data['proje_id'];
                $proje_id = $data['proje_id'];
            }else{
                $this->Proje->create();
            }

            $yuklenenfile = array_key_exists('yuklenenfile', $data) ? $data['yuklenenfile'] : false;
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $saveData = array(
                'baslik' => $baslik,
                'aciklama' => $aciklama,
                'location_name' => $data['location'],
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude']
            );

            if($this->Proje->save($saveData)){
                if($proje_id == 0){
                    $proje_id = $this->Proje->getInsertID();
                }

                $hataRes = 0;
                if($yuklenenfile){
                    foreach($yuklenenfile as $value){
                        $asilpath = 'img/gecici/'.$value;
                        if(is_file($asilpath)){
                            $fileType = pathinfo($value, PATHINFO_EXTENSION);
                            $asilname = date('YmdHis').uniqid();
                            $name = $asilname.'.'.$fileType;
                            $nameThumb = $asilname.'_thumb.'.$fileType;
                            $this->ProjeResim->create();
                            $this->ProjeResim->save(array('proje_id'=>$proje_id));
                            $lastId = $this->ProjeResim->getLastInsertID();
                            $path = 'img/proje/'.$proje_id.'/'.$name;
                            $pathThumb = 'img/proje/'.$proje_id.'/'.$nameThumb;
                            //image Resize
                            $manipulator = new ImageManipulator($asilpath);
                            $newImage = $manipulator->resample(800, 600, false);
                            if($manipulator->save($path)){
                                $this->ProjeResim->id = $lastId;
                                $this->ProjeResim->save(array('path'=>$path));
                                $manipulator = new ImageManipulator($path);
                                $newImage = $manipulator->resample(200, 200);
                                if($manipulator->save($pathThumb)){
                                    $this->ProjeResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                    if(file_exists($asilpath)){
                                        unlink($asilpath);
                                    }
                                }
                            }else{
                                $hataRes++;
                                $this->ProjeResim->deleteAll(array('id'=>$lastId));
                            }
                        }else{
                            $hataRes++;
                        }
                    }
                }

                if($hataRes>0){
                    $return['mesaj'] = 'Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.';
                    $return['link'] = Router::url('/',true).'admins/projeedit?proje_id='.$proje_id;
                    echo json_encode($return);
                    exit();
                }else{
                    $return['hata'] = false;
                    $return['mesaj'] = 'Proje başarıyla kaydedildi.';
                    $return['link'] = Router::url('/',true).'admins/projeedit?proje_id='.$proje_id;
                    echo json_encode($return);
                    exit();
                }
            }else{
                $return['mesaj'] = 'Bir hata meydana geldi. Lütfen tekrar deneyin.';
                $return['link'] = false;
                echo json_encode($return);
                exit();
            }
        }else{
            $return['mesaj'] = 'Hata!!!';
            $return['link'] = Router::url('/',true);
            echo json_encode($return);
            exit();
        }
    }

    // Projeler Son

    // Danismanler Bas
    function danismanlar(){
        $danismanlar = $this->Danisman->find('all');
        $this->set('danismanlar',$danismanlar);
    }

    function danismanedit(){
        $get = $this->request->query;
        if(array_key_exists('id',$get)){
            $danismans = $this->Danisman->find('first',array('conditions'=>array('Danisman.id'=>$get['id']), 'contain'=>array('DanismanIletisim'=>array('IlanIletisimType'))));

            if(!$danismans){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Aramış olduğunuz danışman bulunmamaktadır.','default', array('class'=>'alert alert-danger'));
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'danismanlar')
                );
            }
            $this->set('danismans',$danismans);

            $IlanIletisimType = $this->IlanIletisimType->find('all',array('order'=>array('id'=>'ASC')));
            $this->set('IlanIletisimType', $IlanIletisimType);

            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }
    }

    function danismanupgrade(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            if(array_key_exists('danisman_id',$data) && $this->Danisman->findById($data['danisman_id'])){
                $danismanId = $data['danisman_id'];

                $yuklenenfile = array_key_exists('yuklenenfile',$data)?$data['yuklenenfile']:false;
                $saveData = array(
                    'adi'=>$data['adi'],
                    'soyadi'=>$data['soyadi'],
                    'hakkinda'=>$data['hakkinda']
                );

                $this->Danisman->id = $danismanId;
                if($this->Danisman->save($saveData)){
//                    $danismanId = $this->Danisman->getInsertID();

                    if(array_key_exists('iletisimtipi', $data)){
                        foreach ($data['iletisimtipi'] as $ky=>$vl){
                            if($vl != 0){
                                $this->DanismanIletisim->create();
                                $this->DanismanIletisim->save(array('danisman_id'=>$danismanId, 'type'=>$vl, 'iletisim'=>$data['iletisim'][$ky]));
                            }
                        }
                    }

                    $hataRes = 0;
                    if($yuklenenfile){
                        foreach($yuklenenfile as $value){
                            $asilpath = 'img/gecici/'.$value;
                            if(is_file($asilpath)){
                                $fileType = pathinfo($value, PATHINFO_EXTENSION);
                                $asilname = date('YmdHis').uniqid();
                                $name = $asilname.'.'.$fileType;
                                $path = 'img/danisman/'.$danismanId.'/'.$name;
                                //image Resize
                                $manipulator = new ImageManipulator($asilpath);
                                $newImage = $manipulator->resample(800, 600, false);
                                if($manipulator->save($path)){
                                    $this->Danisman->id = $danismanId;
                                    $this->Danisman->save(array('resim'=>$path));
                                    if(file_exists($asilpath)){
                                        unlink($asilpath);
                                    }
                                }
                            }else{
                                $hataRes++;
                            }
                        }
                    }

                    if($hataRes>0){
                        $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                    }else{
                        $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Danisman başarıyla eklendi.','default', array('class'=>'alert alert-success'));
                    }
                }else{
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Danisman düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'danismanedit?id='.$danismanId)
                );
            }else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Danisman düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'danismanlar')
                );
            }
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Danisman düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'danismanlar')
            );
        }
    }

    function danismandeleteresim(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $danismanId = $this->request->data('danismanId');
            $resim = $this->Danisman->find('first',array('conditions'=>array('id'=>$danismanId)));
            $fileRes = new File($resim['Danisman']['resim']);
            $fileRes->delete();
            $this->Danisman->id = $danismanId;
            if($this->Danisman->save(array('resim'=>''))){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
        exit();
    }

    function danismansil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $danismanId = $this->request->data('danismanId');
            $this->Danisman->deleteAll(array('id'=>$danismanId));
            $return['hata'] = false;
        }
        echo json_encode($return);
        exit();
    }

    function danismanyeni(){
        $IlanIletisimType = $this->IlanIletisimType->find('all',array('order'=>array('id'=>'ASC')));
        $this->set('IlanIletisimType', $IlanIletisimType);
    }

    function danismanadd(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;

            $yuklenenfile = array_key_exists('yuklenenfile',$data)?$data['yuklenenfile']:false;
            $saveData = array(
                'adi'=>$data['adi'],
                'soyadi'=>$data['soyadi'],
                'hakkinda'=>$data['hakkinda']
            );

            $this->Danisman->create();
            if($this->Danisman->save($saveData)){
                $danismanId = $this->Danisman->getInsertID();

                if(array_key_exists('iletisimtipi', $data)){
                    foreach ($data['iletisimtipi'] as $ky=>$vl){
                        if($vl != 0){
                            $this->DanismanIletisim->create();
                            $this->DanismanIletisim->save(array('danisman_id'=>$danismanId, 'type'=>$vl, 'iletisim'=>$data['iletisim'][$ky]));
                        }
                    }
                }

                $hataRes = 0;
                if($yuklenenfile){
                    foreach($yuklenenfile as $value){
                        $asilpath = 'img/gecici/'.$value;
                        if(is_file($asilpath)){
                            $fileType = pathinfo($value, PATHINFO_EXTENSION);
                            $asilname = date('YmdHis').uniqid();
                            $name = $asilname.'.'.$fileType;
                            $path = 'img/danisman/'.$danismanId.'/'.$name;
                            //image Resize
                            $manipulator = new ImageManipulator($asilpath);
                            $newImage = $manipulator->resample(800, 600, false);
                            if($manipulator->save($path)){
                                $this->Danisman->id = $danismanId;
                                $this->Danisman->save(array('resim'=>$path));
                                if(file_exists($asilpath)){
                                    unlink($asilpath);
                                }
                            }
                        }else{
                            $hataRes++;
                        }
                    }
                }

                if($hataRes>0){
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }else{
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Danisman başarıyla eklendi.','default', array('class'=>'alert alert-success'));
                }
            }else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Danisman düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'danismanedit?id='.$danismanId)
            );
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Danisman düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'danismanlar')
            );
        }
    }

    function danismaniletisimsil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $iletisimId = $this->request->data('iletisimId');
            if($this->DanismanIletisim->delete($iletisimId)){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
        exit();
    }

    // Danisnam İşlemeri Son
    public function getIlce(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $il = $this->request->data('il');
//            $ilce = $this->Ilce->find('all',array('conditions'=>array('sehir_id'=>$il),'order'=>array('ilce_adi'=>'ASC')));
            $ilce = $this->Ilce->findAllBySehirId($il);
            echo json_encode($ilce);
        }else{
            echo json_encode(false);
        }
    }

    public function getSemt(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ilce = $this->request->data('ilce');
//            $semt = $this->Semt->find('all',array('conditions'=>array('ilce_id'=>$ilce),'order'=>array('semt_adi','ASC')));
            $semt = $this->Semt->findAllByIlceId($ilce);
            echo json_encode($semt);
        }else{
            echo json_encode(false);
        }
    }

    public function getMahalle(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $semt = $this->request->data('semt');
//            $mahalle = $this->Mahalle->find('all',array('conditions'=>array('semt_id'=>$semt),'order'=>array('mahalle_adi','ASC')));
            $mahalle = $this->Mahalle->findAllBySemtId($semt);
            echo json_encode($mahalle);
        }else{
            echo json_encode(false);
        }
    }

    public function ilandandanismansil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $tablo = $this->request->data('tablo');
            $dId = $this->request->data('dId');
            if($this->$tablo->delete(array('id'=>$dId))){
                $return['hata'] = false;
            }
        }

        echo json_encode($return);
    }

    public function resimSirala(){
        $this->autoRender = false;
        $return = array('hata'=>false);
        if($this->request->is('post')){
            $tablo = $this->request->data('tablo');
            $resimler = $this->request->data('resimler');
            $i = 0;
            foreach ($resimler as $row){
                $resId = explode('_',$row);
                $this->$tablo->id = $resId[1];
                $this->$tablo->save(array('sira'=>$i));
                $i++;
            }
        }

        echo json_encode($return);
    }

    function ilanyeni(){
        $get = $this->request->query;
        $tip = array_key_exists('tip', $get)?$get['tip']:false;
        if($tip && in_array($tip, array(1,2,3))){
            $il = $this->Sehir->find('all',array('order'=>'sehir_adi ASC'));
            $this->set('il',$il);
            $danismanlar = $this->Danisman->find('all',array('order'=>array('adi','soyadi')));
            $this->set('danismanlar', $danismanlar);
            $this->set('ilan_tipi', $tip);
            $ozellikler = $this->Ozellik->find('all',array('order'=>array('ozellik')));
            $this->set('ozellikler',$ozellikler);
        }else{
            $this->redirect(array('controller'=>'admins','action'=>'ilanlar'));
        }

    }

    function ilankaydet(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        $saveData = array();
        if($this->request->is('post')){
            $data = $this->request->data;
            if(!array_key_exists('ilan_id', $data)){
                $return['mesaj'] = 'İlan eklerken bir hata meydana geldi. Lütfen tekrar deneyin.';
                $return['link'] = Router::url('/',true).'admins';
                echo json_encode($return);
                exit();
            }

            $yuklenenfile = array_key_exists('yuklenenfile',$data)?$data['yuklenenfile']:false;
            $ilanId = 0;
            if($data['ilan_id'] != 0){
                $this->Ilan->id = $data['ilan_id'];
                $ilanId = $data['ilan_id'];
            }else{
                if(!array_key_exists('ilan_tipi', $data) || !in_array($data['ilan_tipi'], array(1,2,3))){
                    $return['mesaj'] = 'İlan eklerken bir hata meydana geldi. Lütfen tekrar deneyin.';
                    $return['link'] = Router::url('/',true).'admins';
                    echo json_encode($return);
                    exit();
                }
                $this->Ilan->create();
                $saveData['user_id'] = $this->Session->read('UserId');
            }

            $saveData = array_merge($saveData,array(
                'ilan_tipi'=>$data['ilan_tipi'],
                'baslik'=>$data['baslik'],
                'aciklama'=>$data['aciklama'],
                'kredi'=>$data['kredi'],
                'fiyat'=>$data['fiyat'],
                'sat_kir'=>$data['sat_kir'],
                'mal_name'=>$data['malName'],
                'mal_tel'=>$data['malTelNo'],
                'mal_fiyat'=>$data['malFiyat'],
                'sehir_id' =>$data['il'],
                'ilce_id' => array_key_exists('ilce', $data)?$data['ilce']:null,
                'semt_id' => array_key_exists('semt', $data)?$data['semt']:null,
                'mahalle_id' => array_key_exists('mahalle', $data)?$data['mahalle']:null,
                'location_name' => $data['location'],
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude'],
                'harita'=>array_key_exists('harita',$data)?1:0
            ));

            if($this->Ilan->save($saveData)){
                if($ilanId == 0){
                    $ilanId = $this->Ilan->getInsertID();
                }

                $ilanBilgi = $this->Ilan->read(null,$ilanId);
                $this->Ilan->id = $ilanId;
                $this->Ilan->set('ilan_no','MG-'.$ilanBilgi['Ilan']['ilan_no_id']);
                $this->Ilan->save();
                $durum = false;

                if($ilanBilgi['Ilan']['ilan_tipi'] == 1){
                    $KonutBilgi = $this->KonutBilgi->findByIlanId($ilanId);
                    if($KonutBilgi){
                        $this->KonutBilgi->id = $KonutBilgi['KonutBilgi']['id'];
                    }else{
                        $this->KonutBilgi->create();
                    }

                    $saveArray = array(
                        'ilan_id' => $ilanId,
                        'oda_sayisi'=>$data['oda'],
                        'banyo_sayisi'=>$data['banyo'],
                        'm_kare'=>$data['m2'],
                        'kat'=>$data['kat'],
                        'bina_kat'=>$data['binakat']
                    );

                    $durum = $this->KonutBilgi->save($saveArray);
                }else if($ilanBilgi['Ilan']['ilan_tipi'] == 2){
                    $IsyeriBilgi = $this->IsyeriBilgi->findByIlanId($ilanId);
                    if($IsyeriBilgi){
                        $this->IsyeriBilgi->id = $IsyeriBilgi['IsyeriBilgi']['id'];
                    }else{
                        $this->IsyeriBilgi->create();
                    }

                    $saveArray = array(
                        'ilan_id' => $ilanId,
                        'oda_sayisi'=>$data['oda'],
                        'banyo_sayisi'=>$data['banyo'],
                        'm_kare'=>$data['m2'],
                        'kat'=>$data['kat'],
                        'bina_kat'=>$data['binakat']
                    );

                    $durum = $this->IsyeriBilgi->save($saveArray);
                }else if($ilanBilgi['Ilan']['ilan_tipi'] == 3){
                    $ArsaBilgi = $this->ArsaBilgi->findByIlanId($ilanId);
                    if($ArsaBilgi){
                        $this->ArsaBilgi->id = $ArsaBilgi['ArsaBilgi']['id'];
                    }else{
                        $this->ArsaBilgi->create();
                    }

                    $saveArray = array(
                        'ilan_id' => $ilanId,
                        'imar_durum' => $data['imar'],
                        'm_kare' => $data['m2'],
                        'ada' => $data['ada'],
                        'parsel' => $data['parsel'],
                        'tapu' => $data['tapu']
                    );

                    $durum = $this->ArsaBilgi->save($saveArray);
                }

                if(!$durum){
                    $return['mesaj'] = 'İlan eklerken bir hata meydana geldi. Lütfen tekrar deneyin.';
                    $return['link'] = Router::url('/',true).'admins/ilanedit?ilan_id='.$ilanId;
                    echo json_encode($return);
                    exit();
                }

                // Danisman Ekle
                $danisman = array_key_exists('danisman',$data)?$data['danisman']:false;
                if($danisman){
                    foreach ($danisman as $dan){
                        $ilandanisman = $this->IlanDanisman->findByIlanIdAndDanismanId($ilanId, $dan);
                        if($dan != 0 && empty($ilandanisman)){
                            $this->IlanDanisman->create();
                            $this->IlanDanisman->save(array('ilan_id'=>$ilanId, 'danisman_id'=>$dan));
                        }
                    }
                }
                // Danisman Ekle SON

                // Ozellik Ekle
                $this->IlanOzellik->deleteAll(array('ilan_id'=>$ilanId));
                $ozellik = array_key_exists('ozellik',$data)?$data['ozellik']:false;
                if($ozellik){
                    $ozellik = array_keys($ozellik);
                    foreach ($ozellik as $ozl){
                        $this->IlanOzellik->create();
                        $this->IlanOzellik->save(array('ilan_id'=>$ilanId, 'ozellik_id'=>$ozl));
                    }
                }
                // Ozellik Ekle SON

                // Ilan Resim Ekle
                $hataRes = 0;
                if($yuklenenfile){
                    foreach($yuklenenfile as $value){
                        $asilpath = 'img/gecici/'.$value;
                        if(is_file($asilpath)){
                            $fileType = pathinfo($value, PATHINFO_EXTENSION);
                            $asilname = date('YmdHis').uniqid();
                            $name = $asilname.'.'.$fileType;
                            $nameThumb = $asilname.'_thumb.'.$fileType;
                            $this->IlanResim->create();
                            $this->IlanResim->save(array('ilan_id'=>$ilanId));
                            $lastId = $this->IlanResim->getLastInsertID();
                            $path = 'img/ilan/'.$ilanId.'/'.$name;
                            $pathThumb = 'img/ilan/'.$ilanId.'/'.$nameThumb;
                            //image Resize
                            $manipulator = new ImageManipulator($asilpath);
                            $newImage = $manipulator->resample(1024, 1024, false);
                            if($manipulator->save($path)){
                                $this->IlanResim->id = $lastId;
                                $this->IlanResim->save(array('path'=>$path));
                                $manipulator = new ImageManipulator($path);
                                $newImage = $manipulator->resample(200, 200);
                                if($manipulator->save($pathThumb)){
                                    $this->IlanResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                    if(file_exists($asilpath)){
                                        unlink($asilpath);
                                    }
                                }
                            }else{
                                $hataRes++;
                                $this->IlanResim->deleteAll(array('id'=>$lastId));
                            }
                        }else{
                            $hataRes++;
                        }
                    }
                }
                // Ilan Resim Son

                if($hataRes>0){
                    $return['mesaj'] = 'Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.';
                    $return['link'] = Router::url('/',true).'admins/ilanedit?ilan_id='.$ilanId;
                    echo json_encode($return);
                    exit();
                }else{
                    $return['hata'] = false;
                    $return['mesaj'] = 'İlan başarıyla kaydedildi.';
                    $return['link'] = Router::url('/',true).'admins/ilanedit?ilan_id='.$ilanId;
                    echo json_encode($return);
                    exit();
                }
            }else{
                $return['mesaj'] = 'Bir hata meydana geldi. Lütfen tekrar deneyin.';
                $return['link'] = false;
                echo json_encode($return);
                exit();
            }
        }else{
            $return['mesaj'] = 'Hata!!!';
            $return['link'] = Router::url('/',true);
            echo json_encode($return);
            exit();
        }
    }

    public function ilanedit(){
        $get = $this->request->query;
        if(array_key_exists('ilan_id',$get)){
            $conditions = array('Ilan.id'=>$get['ilan_id']);
            if($this->Session->read('UserGroup') != 1){
                $conditions['Ilan.user_id'] = $this->Session->read('UserId');
            }
            $ilan = $this->Ilan->find('first',array(
                'conditions'=>$conditions,
                'contain'=>array('KonutBilgi','IsyeriBilgi','ArsaBilgi','IlanResim','IlanDanisman'=>array('Danisman'), 'IlanOzellik')
                ));
            if(empty($ilan)){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız ilan bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
                $this->redirect(array('controller'=>'admins','action'=>'ilanlar'));
            }
            $this->set('ilan',$ilan);

            $il = $this->Sehir->find('all',array('order'=>'id ASC'));
            $this->set('il',$il);
            $ilce = $this->Ilce->find('all',array('conditions'=>array('sehir_id'=>$ilan['Ilan']['sehir_id']),'order'=>'id ASC'));
            $this->set('ilce',$ilce);
            $semt = $this->Semt->find('all',array('conditions'=>array('ilce_id'=>$ilan['Ilan']['ilce_id']),'order'=>'id ASC'));
            $this->set('semt',$semt);
            $mahalle = $this->Mahalle->find('all',array('conditions'=>array('semt_id'=>$ilan['Ilan']['semt_id']),'order'=>'id ASC'));
            $this->set('mahalle',$mahalle);

            $danismanlar = $this->Danisman->find('all',array('order'=>array('adi','soyadi')));
            $this->set('danismanlar', $danismanlar);

            $ozellikler = $this->Ozellik->find('all',array('order'=>array('ozellik')));
            $this->set('ozellikler',$ozellikler);
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız ilan bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
            $this->redirect(array('controller'=>'admins','action'=>'index'));
        }
    }

    function ilandeleteresim(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->IlanResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['IlanResim']['path']);
            $fileRes->delete();
            $fileResThumb = new File($resim['IlanResim']['paththumb']);
            if($fileResThumb){
                $fileResThumb->delete();
            }

            if($this->IlanResim->deleteAll(array('id'=>$ResId))){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
        exit();
    }

    function ilansil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $ilanId = $this->request->data('ilanId');
            $ilan = $this->Ilan->findById($ilanId);
            if($ilan['Ilan']['user_id'] != $this->Session->read('UserId') && $this->Session->read('UserGroup') != 1){
                echo json_encode(array('hata'=>true, 'message'=>'Bu ilanı silme yetkiniz yoktur.'));
            }else{
                $ilanRes = $this->IlanResim->findAllByIlanId($ilanId);
                foreach($ilanRes as $row){
                    $fileRes = new File($row['IlanResim']['path']);
                    $fileRes->delete();
                    $fileResThumb = new File($row['IlanResim']['paththumb']);
                    if($fileResThumb){
                        $fileResThumb->delete();
                    }
                }
                $this->IlanResim->deleteAll(array('ilan_id'=>$ilanId));
                if($ilan['Ilan']['ilan_tipi'] == 1){
                    $this->KonutBilgi->deleteAll(array('ilan_id'=>$ilanId));
                }else if($ilan['Ilan']['ilan_tipi'] == 2){
                    $this->IsyeriBilgi->deleteAll(array('ilan_id'=>$ilanId));
                }else if($ilan['Ilan']['ilan_tipi'] == 3){
                    $this->ArsaBilgi->deleteAll(array('ilan_id'=>$ilanId));
                }

                $this->IlanOzellik->deleteAll(array('ilan_id'=>$ilanId));
                $this->Ilan->deleteAll(array('id'=>$ilanId));
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
        exit();
    }

    function ilanlar(){
        $get = $this->request->query;
        $conditions = $contain = array();
        $title = "";

        if($this->Session->read('UserGroup') != 1){
            $conditions['Ilan.user_id'] = $this->Session->read('UserId');
        }

        if(array_key_exists('danisman_id',$get)){
            $contain[] = 'IlanDanisman';
            $conditions[] = "Ilan.id IN (SELECT ilan_id FROM em_ilan_danisman WHERE danisman_id = ".$get['danisman_id'].")";
            $danisman = $this->Danisman->read(null,$get['danisman_id']);
            if(empty($danisman)){
                $this->redirect(array('controller'=>'admins','action'=>'ilanlar'));
            }
            $title .= '<b>'.$danisman['Danisman']['adi'].' '.$danisman['Danisman']['soyadi'].'</b> Danışman ';
        }

        $ilan_tipi = array_key_exists('ilan_tipi',$get)?$get['ilan_tipi']:0;
        if($ilan_tipi && in_array($ilan_tipi, array(1,2,3))){
            $conditions['ilan_tipi'] = $ilan_tipi;
            $title .= $this->IlanTipArr[$ilan_tipi].' ';
        }

        $ilanlar = $this->Ilan->find('all',array('conditions'=>$conditions, 'contain'=>$contain,'order'=>array('update_tarihi')));
        $this->set('ilanlar',$ilanlar);

        $this->set('ilan_tipi',$ilan_tipi);
        $title .= 'İlanları';
        $this->set('title',$title);
    }

    // User islemleri BAS
    function userlar(){
        $users = $this->User->find('all');
        $this->set('users',$users);
    }

    function useryeni(){}

    function userkaydet(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $data = $this->request->data;

            $user_id = 0;
            if(array_key_exists('user_id', $data)){
                $user = $this->User->read(null, $data['user_id']);
                if(empty($user)){
                    $return['mesaj'] = 'Kullanıcı eklerken bir hata meydana geldi. Lütfen tekrar deneyin.';
                    $return['link'] = Router::url('/',true).'admins';
                    echo json_encode($return);
                    exit();
                }
                $this->User->id = $data['user_id'];
                $user_id = $data['user_id'];
                $saveData = array(
                    'name'=>$data['name'],
                    'pass'=>Security::hash($data['pass'], 'sha1', true),
                    'keke'=>$data['pass'],
                    'group'=>$data['group'],
                    'durum'=>$data['durum']
                );
            }else{
                $usernameVarmi = $this->User->find('all',array('conditions'=>array('username'=>$data['username'])));
                if($usernameVarmi){
                    $return['mesaj'] = 'Kullanıcı Adı kullanılmakta. Lütfen başka bir kullanıcı adı giriniz.';
                    $return['link'] = false;
                    echo json_encode($return);
                    exit();
                }
                $emailVarmi = $this->User->find('all',array('conditions'=>array('email'=>$data['email'])));
                if($emailVarmi){
                    $return['mesaj'] = 'Kullanıcı Mail kullanılmakta. Lütfen başka bir kullanıcı mail giriniz.';
                    $return['link'] = false;
                    echo json_encode($return);
                    exit();
                }

                $this->Ilan->create();
                $saveData = array(
                    'name'=>$data['name'],
                    'username'=>$data['username'],
                    'email'=>$data['email'],
                    'pass'=>Security::hash($data['pass'], 'sha1', true),
                    'keke'=>$data['pass'],
                    'group'=>$data['group'],
                    'durum'=>$data['durum']
                );
            }



            if($this->User->save($saveData)){
                if($user_id == 0){
                    $user_id = $this->User->getInsertID();
                }
                $return['hata'] = false;
                $return['mesaj'] = 'Kullanıcı başarıyla kaydedildi.';
                $return['link'] = Router::url('/',true).'admins/useredit?user_id='.$user_id;
                echo json_encode($return);
                exit();
            }else{
                $return['mesaj'] = 'Bir hata meydana geldi. Lütfen tekrar deneyin.';
                $return['link'] = false;
                echo json_encode($return);
                exit();
            }
        }else{
            $return['mesaj'] = 'Hata!!!';
            $return['link'] = Router::url('/',true);
            echo json_encode($return);
            exit();
        }
    }

    function useredit(){
        $get = $this->request->query;
        if(array_key_exists('user_id',$get)){
            $user = $this->User->find('first',array(
                'conditions'=>array('id'=>$get['user_id'])
            ));
            if(empty($user)){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kullanıcı bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
                $this->redirect(array('controller'=>'admins','action'=>'index'));
            }
            $this->set('user',$user);
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kullanıcı bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
            $this->redirect(array('controller'=>'admins','action'=>'index'));
        }
    }
    // User islemleri Son

    public function ozellikler(){
        $ozellikler = $this->Ozellik->find('all',array('order'=>array('ozellik')));
        $this->set('ozellikler', $ozellikler);
    }

    public function ozellikkaydet(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $data = $this->request->data;
            if($data['ozellik_id'] != 0){
                $ozellik = $this->Ozellik->read(null, $data['ozellik_id']);
                if(empty($ozellik)){
                    echo json_encode($return);
                    exit();
                }
                $this->Ozellik->id = $data['ozellik_id'];
            }else{
                $this->Ozellik->create();
            }

            if($this->Ozellik->save(array('ozellik'=>$data['ozellik']))){
                $return['hata'] = false;
            }
        }

        echo json_encode($return);
        exit();
    }

    public function ozellikgetir(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $data = $this->request->data;
            $ozellik = $this->Ozellik->read(null, $data['oId']);
            if($ozellik){
                $return['ozellik_id'] = $ozellik['Ozellik']['id'];
                $return['ozellik'] = $ozellik['Ozellik']['ozellik'];
                $return['hata'] = false;
            }
        }

        echo json_encode($return);
        exit();
    }

    public function ozelliksil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $data = $this->request->data;
            $ozellik = $this->Ozellik->read(null, $data['oId']);
            if($ozellik){
                $this->IlanOzellik->deleteAll(array('ozellik_id'=>$data['oId']));
                $this->Ozellik->deleteAll(array('id'=>$data['oId']));
                $return['hata'] = false;
            }
        }

        echo json_encode($return);
        exit();
    }
}