<?php
class Emlak2sController extends AppController{
    var $uses = array('Ilan','IlanResim', 'KonutBilgi', 'ArsaBilgi', 'IsyeriBilgi', 'IlanDanisman','Konut','KonutResim','KonutIletisim', 'KonutYer', 'KonutLocation', 'Isyeri', 'IsyeriYer','IsyeriResim','IsyeriLocation','Arsa','ArsaYer','ArsaResim','ArsaLocation','Tip','Sehir','Ilce','Mahalle','Semt','IlanNo',
        'ArsaIletisim','IlanIletisim','IlanIletisimType', 'User', 'Haber', 'HaberResim', 'Proje', 'ProjeResim', 'ProjeLocation', 'Danisman', 'DanismanIletisim','KonutDanisman','IsyeriDanisman','ArsaDanisman');
    public $components = array('Paginator','HtmlMeta');
    var $layout = "emlak2";

    public function index(){
        $sehir = $this->Sehir->find('all',array('order'=>array('sehir_adi')));
        $this->set('sehirler',$sehir);

        $mapIlan = $this->Ilan->find('all',
                array(
                    'fields'=>array('*'),
                    'contain'=>array('IlanResim'=>array('limit'=>1)),
                    'order'=>array('Ilan.update_tarihi'=>'DESC')
                )
            );
        $this->set('mapIlan',$mapIlan);
    }

    public function searchmap(){
        $this->autoRender = false;
        echo json_encode($this->request->data);
        exit();
    }

    public function ilan(){
        $get = $this->request->query;
        if(array_key_exists('ilan_id',$get)){
            $ilan = $this->Ilan->find('first',array(
                'conditions'=>array('Ilan.id'=>$get['ilan_id']),
                'contain'=>array('KonutBilgi','IsyeriBilgi','ArsaBilgi','IlanResim','IlanDanisman'=>array('Danisman'=>array('DanismanIletisim'=>array('IlanIletisimType'))), 'Sehir', 'Ilce', 'Semt', 'Mahalle','IlanOzellik'=>array('Ozellik'))
            ));
            if(empty($ilan)){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız ilan bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
                $this->redirect(array('controller'=>'emalak2s','action'=>'index'));
            }
            $this->set('ilan',$ilan);

        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız ilan bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
            $this->redirect(array('controller'=>'admins','action'=>'index'));
        }
    }

    public function ilanlar_grid(){
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->passedArgs = $data;
        }else{
            $data = $this->passedArgs;
        }
        $limit = 10;

        $conditions = $order = $sehirCon = $ilceCon = $semtCon = $mahalleCon = array();
        $sval = '';
        if(array_key_exists('sval', $data)) {
            $sval = $data['sval'];
            $this->set('sval',$sval);
            $conditions['OR'] = array("Ilan.baslik LIKE '%$sval%'",
                "Ilan.aciklama LIKE '%$sval%'",
                "Ilan.ilan_no LIKE '%$sval%'",
                "Ilan.location_name LIKE '%$sval%'",
                "Sehir.sehir_adi LIKE '%$sval%'",
                "Ilce.ilce_adi LIKE '%$sval%'",
                "Semt.semt_adi LIKE '%$sval%'",
                "Mahalle.mahalle_adi LIKE '%$sval%'"
            );
        }

        if(array_key_exists('tarih', $data)){
            if($data['tarih']=='desc'){
                $order['Ilan.tarih'] = 'DESC';
            }else if($data['tarih']=='asc'){
                $order['Ilan.tarih'] = 'ASC';
            }
        }else if(array_key_exists('fiyat', $data)){
            if($data['fiyat']=='desc'){
                $order['Ilan.fiyat'] = 'DESC';
            }else if($data['fiyat']=='asc'){
                $order['Ilan.fiyat'] = 'ASC';
            }
        }else{
            $order['Ilan.update_tarihi'] = 'DESC';
        }

        // İlan Tipi
        if(array_key_exists('tip',$data) && $data['tip'] != 0){
            $conditions['Ilan.ilan_tipi'] = $data['tip'];
        }
        // İlan Satılık-Kiralik
        if(array_key_exists('satkir',$data) && $data['satkir'] != 0){
            $conditions['Ilan.sat_kir'] = $data['satkir'];
        }
        // İlan Şehir
        if(array_key_exists('il',$data) && $data['il'] != 0){
            $conditions['Ilan.sehir_id'] = $data['il'];
        }

        //İlan Fiyatı
        if(array_key_exists('fiy1',$data) && $data['fiy1'] != 0){
            $conditions[] = "Ilan.fiyat >= ".$data['fiy1'];
        }
        if(array_key_exists('fiy2',$data) && $data['fiy2'] != 0){
            $conditions[] = "Ilan.fiyat <= ".$data['fiy2'];
        }

        //İlan Metre Kare
        if(array_key_exists('m1',$data) && $data['m1'] != 0){
            $conditions[] = "Ilan.m_kare >= ".$data['m1'];
        }
        if(array_key_exists('m2',$data) && $data['m2'] != 0){
            $conditions[] = "Ilan.m_kare <= ".$data['m2'];
        }

        $ilce = array_key_exists('ilce', $data)?$data['ilce']:0;
        $semt = array_key_exists('semt', $data)?$data['semt']:0;
        $mahalle = array_key_exists('mahalle', $data)?$data['mahalle']:0;
        // İlan İlçe
        if($ilce != 0){
            $conditions['Ilan.ilce_id'] = $ilce;
        }
        // İlan Semt
        if($semt != 0){
            $conditions['Ilan.semt_id'] = $semt;
        }
        // İlan Mahalle
        if($mahalle != 0){
            $conditions['Ilan.mahalle_id'] = $mahalle;
        }

        $this->paginate = array(
            'fields'=>array('*'),
            'contain'=>array('KonutBilgi','IsyeriBilgi','ArsaBilgi','IlanResim'=>array('order'=>array('sira'=>'ASC'),'limit'=>1),'Sehir','Ilce','Semt','Mahalle'),
            'conditions'=>$conditions,
            'limit'=>$limit,
            'order'=>$order
        );
        $ilanlar = $this->paginate('Ilan');
        $this->set('ilanlar',$ilanlar);
        $this->set('sTip',(array_key_exists('tip',$data)?$data['tip']:0));
        $this->set('sSatKir',(array_key_exists('satkir',$data)?$data['satkir']:0));
        $this->set('sval',$sval);
    }

    public function ilanlar_masonry(){
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->passedArgs = $data;
        }else{
            $data = $this->passedArgs;
        }
        $limit = 2;

        $conditions = $order = $sehirCon = $ilceCon = $semtCon = $mahalleCon = array();
        $sval = '';
        if(array_key_exists('sval', $data)) {
            $sval = $data['sval'];
            $this->set('sval',$sval);
            $conditions['OR'] = array("Ilan.baslik LIKE '%$sval%'",
                "Ilan.aciklama LIKE '%$sval%'",
                "Ilan.ilan_no LIKE '%$sval%'",
                "Ilan.location_name LIKE '%$sval%'",
                "Sehir.sehir_adi LIKE '%$sval%'",
                "Ilce.ilce_adi LIKE '%$sval%'",
                "Semt.semt_adi LIKE '%$sval%'",
                "Mahalle.mahalle_adi LIKE '%$sval%'"
            );
        }

        if(array_key_exists('tarih', $data)){
            if($data['tarih']=='desc'){
                $order['Ilan.tarih'] = 'DESC';
            }else if($data['tarih']=='asc'){
                $order['Ilan.tarih'] = 'ASC';
            }
        }else if(array_key_exists('fiyat', $data)){
            if($data['fiyat']=='desc'){
                $order['Ilan.fiyat'] = 'DESC';
            }else if($data['fiyat']=='asc'){
                $order['Ilan.fiyat'] = 'ASC';
            }
        }else{
            $order['Ilan.update_tarihi'] = 'DESC';
        }

        // İlan Tipi
        if(array_key_exists('tip',$data) && $data['tip'] != 0){
            $conditions['Ilan.ilan_tipi'] = $data['tip'];
        }
        // İlan Satılık-Kiralik
        if(array_key_exists('satkir',$data) && $data['satkir'] != 0){
            $conditions['Ilan.sat_kir'] = $data['satkir'];
        }
        // İlan Şehir
        if(array_key_exists('il',$data) && $data['il'] != 0){
            $conditions['Ilan.sehir_id'] = $data['il'];
        }

        //İlan Fiyatı
        if(array_key_exists('fiy1',$data) && $data['fiy1'] != 0){
            $conditions[] = "Ilan.fiyat >= ".$data['fiy1'];
        }
        if(array_key_exists('fiy2',$data) && $data['fiy2'] != 0){
            $conditions[] = "Ilan.fiyat <= ".$data['fiy2'];
        }

        //İlan Metre Kare
        if(array_key_exists('m1',$data) && $data['m1'] != 0){
            $conditions[] = "Ilan.m_kare >= ".$data['m1'];
        }
        if(array_key_exists('m2',$data) && $data['m2'] != 0){
            $conditions[] = "Ilan.m_kare <= ".$data['m2'];
        }

        $ilce = array_key_exists('ilce', $data)?$data['ilce']:0;
        $semt = array_key_exists('semt', $data)?$data['semt']:0;
        $mahalle = array_key_exists('mahalle', $data)?$data['mahalle']:0;
        // İlan İlçe
        if($ilce != 0){
            $conditions['Ilan.ilce_id'] = $ilce;
        }
        // İlan Semt
        if($semt != 0){
            $conditions['Ilan.semt_id'] = $semt;
        }
        // İlan Mahalle
        if($mahalle != 0){
            $conditions['Ilan.mahalle_id'] = $mahalle;
        }

        $this->paginate = array(
            'fields'=>array('*'),
            'contain'=>array('KonutBilgi','IsyeriBilgi','ArsaBilgi','IlanResim'=>array('order'=>array('sira'=>'ASC'),'limit'=>1),'Sehir','Ilce','Semt','Mahalle'),
            'conditions'=>$conditions,
            'limit'=>$limit,
            'order'=>$order
        );
        $ilanlar = $this->paginate('Ilan');
        $this->set('ilanlar',$ilanlar);
        $this->set('sTip',(array_key_exists('tip',$data)?$data['tip']:0));
        $this->set('sSatKir',(array_key_exists('satkir',$data)?$data['satkir']:0));
        $this->set('sval',$sval);
        $sehir = $this->Sehir->find('all',array('order'=>array('sehir_adi'=>'ASC')));
        $this->set('sehir',$sehir);
    }

    public function GetPagUrl(){
        $this->autoRender = false;
        $data = $this->passedArgs;
        $ekle = '';
        foreach($data as $key=>$val){
            $ekle .= '/'.$key.':'.$val;
        }
        return $ekle;
    }

    public function GetPagUrlWithoutSval(){
        $this->autoRender = false;
        $data = $this->passedArgs;
        $ekle = '';
        foreach($data as $key=>$val){
            if($key != 'sval'){
                $ekle .= '/'.$key.':'.$val;
            }
        }
        return $ekle;
    }
}