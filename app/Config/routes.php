<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'users', 'action' => 'index'));
	Router::connect('/arama/*', array('controller' => 'users', 'action' => 'arama'));
	Router::connect('/arsa/*', array('controller' => 'arsas', 'action' => 'arsa'));
	Router::connect('/yeniarsa/*', array('controller' => 'arsas', 'action' => 'yeniarsa'));
	Router::connect('/konut/*', array('controller' => 'konuts', 'action' => 'konut'));
	Router::connect('/yenikonut/*', array('controller' => 'konuts', 'action' => 'yenikonut'));
	Router::connect('/yeniisyeri/*', array('controller' => 'isyeris', 'action' => 'yeniisyeri'));
	Router::connect('/isyeri/*', array('controller' => 'isyeris', 'action' => 'isyeri'));
	Router::connect('/haber/*', array('controller' => 'habers', 'action' => 'haber'));
	Router::connect('/proje/*', array('controller' => 'projes', 'action' => 'proje'));
	Router::connect('/teknikanaliz/*', array('controller' => 'tekniks', 'action' => 'teknikanaliz'));
	Router::connect('/detaylisearch', array('controller' => 'users', 'action' => 'detaylisearch'));
	Router::connect('/iletisim/*', array('controller' => 'users', 'action' => 'iletisim'));
	Router::connect('/kurumsal/*', array('controller' => 'users', 'action' => 'kurumsal'));
	Router::connect('/giris/*', array('controller' => 'admins', 'action' => 'giris'));
	Router::connect('/logout/*', array('controller' => 'admins', 'action' => 'logout'));
	Router::connect('/konutindex/*', array('controller' => 'konuts', 'action' => 'konutindex'));
	Router::connect('/isyeriindex/*', array('controller' => 'isyeris', 'action' => 'isyeriindex'));
	Router::connect('/arsaindex/*', array('controller' => 'arsas', 'action' => 'arsaindex'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
